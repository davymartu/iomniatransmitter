/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Configurator {

	private final Logger logger = LogManager.getLogger(Configurator.class);
	private static final Configurator instance = new Configurator();
	final String CONFIG_DIR = "config/";
	final String CONFIG_FILE_NAME = "configuration.properties";
	private String inizio;
	private String fine;
	private String timeout;
	private String inputPath;
	
	private boolean closeAtMidnight;
	private Properties prop;

	private ArrayList<String> companies;
	private String passwordKey;

	public String getInizio() {
		return inizio;
	}

	public String getFine() {
		return fine;
	}

	public String getTimeout() {
		return timeout;
	}

	public String getInputPath() {
		return inputPath;
	}
	
	public boolean getCloseAtMidnight() {
		return closeAtMidnight;
	}

	public void setCloseAtMidnight(boolean closeAtMidnight) {
		this.closeAtMidnight = closeAtMidnight;
	}	

	public static Configurator getInstance() {
		if (instance == null) {
			return new Configurator();
		}
		return instance;
	}

	public Configurator(){
		this.caricaDati();
	}

	private void caricaDati(){
		try{
			FileInputStream in = new FileInputStream (this.CONFIG_DIR + CONFIG_FILE_NAME);
			prop = new Properties();
			prop.load(in);
			inizio = prop.getProperty("job.start","00:01");
			fine = prop.getProperty("job.end","23:59");
			timeout = prop.getProperty("job.scan_timeout","10");
			inputPath = prop.getProperty("job.input_dir_path");
			closeAtMidnight = Boolean.parseBoolean(prop.getProperty("job.close_at_midnight","false"));
			setPasswordKey(prop.getProperty("security.password_key"));
			//				String mailList= prop.getProperty("mail_list");
			//
			//				StringTokenizer st = new StringTokenizer(mailList,";");
			//				this.mailList = new ArrayList();
			//				while(st.hasMoreTokens()){
			//					String curVal = st.nextToken();
			//					if(curVal.trim() != "")
			//						this.mailList.add(curVal.trim());
			//				}

			logger.info("***Properties***");
			logger.info("Begin job: " + inizio);
			logger.info("End job: " + fine);
			logger.info("timeout job: " + timeout);
			logger.info("close_at_midnight: " + closeAtMidnight);
			
			//				for(int i = 0 ; i < this.mailList.size();i++){
			//					ProgramLogger.getInstance().setMessaggioInfo("Recipient: " + this.mailList.get(i));
			//				}
			
			StringTokenizer stringTokenizer = new StringTokenizer(prop.getProperty("smtp.companies","default"), ";");		
			companies = new ArrayList<String>();
			while (stringTokenizer.hasMoreElements()) {
				String company = stringTokenizer.nextToken().trim();
				if(!company.isEmpty()) {
					companies.add(company);
					logger.info("found SMTP config: " + company);
				}
			}			
			logger.info("***End of Properties***");
		}catch(FileNotFoundException e){
			logger.error(e.getMessage());
		}catch(IOException e){
			logger.error(e.getMessage());
		}
	}

	public Calendar getInizioJob(){
		Calendar inizioCal = Calendar.getInstance();
		try{

			StringTokenizer st = new StringTokenizer(this.inizio,":");
			inizioCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(st.nextToken()));
			inizioCal.set(Calendar.MINUTE, Integer.parseInt(st.nextToken()));
			return inizioCal;
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		inizioCal.set(Calendar.HOUR_OF_DAY, 0);
		inizioCal.set(Calendar.MINUTE, 0);
		return inizioCal;
	}

	public Calendar getFineJob(){
		Calendar fineCal = Calendar.getInstance();
		try{
			StringTokenizer st = new StringTokenizer(this.fine,":");
			fineCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(st.nextToken()));
			fineCal.set(Calendar.MINUTE, Integer.parseInt(st.nextToken()));
			return fineCal;
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		fineCal.set(Calendar.HOUR_OF_DAY, 0);
		fineCal.set(Calendar.MINUTE,1);
		return fineCal;
	}

	public String getPropValue(String propName,String defaultValue ) {
		
		String propValue = prop.getProperty(propName.toLowerCase(),defaultValue);
		if(propName.toLowerCase().contains("password")){
			logger.debug("Getting property '" + propName + "' with value '*****'");
			try {
				propValue= PasswordUtils.decrypt(propValue);
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}
		}else{
			logger.debug("Getting property '" + propName + "' with value '" + propValue + "'");
		}
		return propValue;
	}
	
	public int getPropValueInt(String propName, String defaultValue ) {
		int propValue = 0;
		try {
			propValue = Integer.parseInt(prop.getProperty(propName.toLowerCase(),defaultValue));
			logger.debug("Getting int property '" + propName + "' with value '" + propValue + "'");
		}catch(Exception e) {
			logger.error(e.getMessage(),e);
			propValue = Integer.parseInt(defaultValue);
		}
		return propValue;
	}

	public String getLogo(String logoCode) {
		String logo= getPropValue("pdf."+logoCode,"");
		if(logo.isEmpty()) {
			logo= getPropValue("pdf.logo_default","LOGO01.jpg");
		}
		return logo;
	}
	
	public ArrayList<String> getSMTPCompanies() {
		return companies;		
	}

	public String getPasswordKey() {
		return passwordKey;
	}

	public void setPasswordKey(String passwordKey) {
		this.passwordKey = passwordKey;
	}
}
