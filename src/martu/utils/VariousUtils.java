/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VariousUtils {
	
	private final static Logger logger = LogManager.getLogger(VariousUtils.class);
	
	public static String transformAttachPath(String attachPath) {
		try {
			String path = attachPath.replaceAll("\\\\", "/");
			try {
				path = path.substring(Configurator.getInstance().getPropValueInt("attachments.substring_index_start","0"));
			}catch(NumberFormatException e) {
				logger.warn(e.getMessage(),e);
			}
			path = Configurator.getInstance().getPropValue("attachments.substring_prefix","").trim() + path.trim();
			logger.debug("transformAttach result:" + path);
			return path;
		}catch(Exception e) {
			logger.error(e.getMessage(),e);
		}
		return attachPath;
	}

}
