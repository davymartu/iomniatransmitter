/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.utils;


import java.io.File;
import java.io.IOException;

import java.net.URL;
import java.net.URLConnection;

import javax.imageio.ImageIO;



public class RemoteImage {

	public static void main(String args[]) throws IOException{
		URL url = new URL("https://storage.googleapis.com/cmsadvcontents/00016/logo.jpg");
		URLConnection conn = url.openConnection();

		//get all headers
//		Map<String, List<String>> map = conn.getHeaderFields();
//		for (Map.Entry<String, List<String>> entry : map.entrySet()) {
//			System.out.println("Key : " + entry.getKey() +
//		                 " ,Value : " + entry.getValue());
//		}

		//get header by 'key'
		String contentType = conn.getHeaderField("Content-Type");
		
		System.out.println(contentType);
//		Image img = ImageIO.read(url);
		
		if(!contentType.contains("application/xml")){
			File f = new File("tmp/logo.jpg");
			ImageIO.write(ImageIO.read(url), "JPG", f);
		}
	}
}
