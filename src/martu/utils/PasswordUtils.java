package martu.utils;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import jcifs.util.Base64;

public class PasswordUtils {
	private static final String ALGHORITM = "AES";
	private static final String KEY = Configurator.getInstance().getPasswordKey(); // 128 bit key
	public static String encrypt(String val)throws Exception{
		try
		{
			String text = val;
			Key aesKey = new SecretKeySpec(KEY.getBytes(), ALGHORITM);
			Cipher cipher = Cipher.getInstance(ALGHORITM);
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(text.getBytes());
			return Base64.encode(encrypted);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	public static String decrypt(String val) throws Exception{
		try {
			Key aesKey = new SecretKeySpec(KEY.getBytes(), ALGHORITM);
			Cipher cipher = Cipher.getInstance(ALGHORITM);
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			String decrypted = new String(cipher.doFinal(Base64.decode(val).clone()));
			return decrypted;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

}
