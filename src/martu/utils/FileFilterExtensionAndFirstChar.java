/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.utils;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

public class FileFilterExtensionAndFirstChar implements FileFilter {

	private String extension;
	private ArrayList<String> prefixArray;

	public FileFilterExtensionAndFirstChar(String extension, ArrayList<String> prefixArray){
		this.extension="."+extension;
		this.prefixArray= prefixArray;
	}

	public boolean accept(File pathname) {
		if( pathname.getName().toLowerCase().endsWith(extension) )
			if (prefixArray.contains(pathname.getName().toUpperCase().substring(0,1)))
				return true;

		return false;
	}
}
