/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.utils;

import java.io.File;
import java.io.IOException;
import java.util.*;


public class CreaCartelle{

	private Calendar calendar;
	private int mese;
	private int anno;
	private String generatedDirPath;
	private boolean out;

	public String getGeneratedDirPath() {
		return generatedDirPath;
	}
	
	public int getMese() {
		return mese;
	}
	
	public int getAnno() {
		return anno;
	}
	
	public CreaCartelle(String path) {
		super();
		this.calendar = new GregorianCalendar();
		this.mese=calendar.get(Calendar.MONTH)+1;
		this.anno=calendar.get(Calendar.YEAR);
		this.generatedDirPath=path+ "/"+anno + "/" + mese + "/";
		try {
			createDir(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean createDir(String path) throws IOException {
		if(!new File(path).exists()) {
			new File(path).mkdirs();
		}
		path=path+ "/"+anno;
		out = new File(path).mkdir();
		path=path+ "/"+mese;
		out = new File(path).mkdir();
		return out;
	}

}
