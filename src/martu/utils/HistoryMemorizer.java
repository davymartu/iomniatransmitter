/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HistoryMemorizer {
	private final Logger logger = LogManager.getLogger(Configurator.class);
	private volatile HashMap<String,Calendar> messageWaitingAttachmentArrivalTime = new HashMap<String,Calendar>();
	private static final HistoryMemorizer instance = new HistoryMemorizer();
	DateFormat defaultDateFmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	public static HistoryMemorizer getInstance() {
		if (instance == null) {
			return new HistoryMemorizer();
		}
		return instance;
	}

	public HistoryMemorizer(){
		
	}

	public HashMap<String, Calendar> getMessageWaitingAttachmentArrivalTime() {
		return messageWaitingAttachmentArrivalTime;
	}

	public void setMessageWaitingAttachmentArrivalTime(HashMap<String, Calendar> messageWaitingAttachmentArrivalTime) {
		this.messageWaitingAttachmentArrivalTime = messageWaitingAttachmentArrivalTime;
	}

	public void addNewMessageWaiting(String name, Calendar fileArrivalTime) {
		
		if(	!this.messageWaitingAttachmentArrivalTime.containsKey(name)) {
			this.messageWaitingAttachmentArrivalTime.put(name,fileArrivalTime);
			logger.debug("Adding " + name + " at "  + defaultDateFmt.format(fileArrivalTime.getTime()));
		};		
	}
}

	
