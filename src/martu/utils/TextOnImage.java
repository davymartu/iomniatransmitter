/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.utils;

import java.awt.*;
import java.awt.font.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;

public class TextOnImage {
	static String text = "hello world";

	public TextOnImage(BufferedImage image) {
		drawText(image);
		File file = save(image);
		show(file);
	}

	private File save(BufferedImage image) {
		String ext = "jpg";   // bmp, gif, png okay in j2se 1.6
		File file = new File("textOnImage." + ext);
		try {
			ImageIO.write(image, ext, file);
		} catch(IOException e) {
			System.out.println("write error: " + e.getMessage());
		}
		return file;
	}

	private void show(File file) {
		BufferedImage image = null;
		try {
			image = ImageIO.read(file);
		} catch(IOException e) {
			System.out.println("read error: " + e.getMessage());
		}
		ImageIcon icon = new ImageIcon(image);
		JOptionPane.showMessageDialog(null, icon, "", -1);
	}
	
	private void drawText(BufferedImage image) {
		Graphics2D g2 = image.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		Font font = g2.getFont().deriveFont(36f);
		g2.setFont(font);
		FontRenderContext frc = g2.getFontRenderContext();
		float width = (float)font.getStringBounds(text, frc).getWidth();
		LineMetrics lm = font.getLineMetrics(text, frc);
		float height = lm.getAscent() + lm.getDescent();
		float x = (image.getWidth() - width)/2f;
		float y = (image.getHeight() + height)/2f - lm.getDescent();
		g2.setPaint(Color.red);
		g2.drawString(text, x, y);
		g2.dispose();
	}
	// Returns a generated image. 
	public static RenderedImage myCreateImage() { 
		int width = 550; int height = 730; 		
		// Create a buffered image in which to draw
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); 
		// Create a graphics contents on the buffered image 
		Graphics2D g2d = bufferedImage.createGraphics(); 
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		Font font = g2d.getFont().deriveFont(12f);
		g2d.setFont(font);
		FontRenderContext frc = g2d.getFontRenderContext();
		//float w = (float)font.getStringBounds(text, frc).getWidth();
		LineMetrics lm = font.getLineMetrics(text, frc);
		//float h = lm.getAscent() + lm.getDescent();
		float x = (bufferedImage.getWidth() - width)/2f;
		float y = (bufferedImage.getHeight() + height)/2f - lm.getDescent();
		g2d.setPaint(Color.red);
		g2d.drawString(text, x, y);
		g2d.dispose();
		return bufferedImage; 
	} 


	public static void main(String[] args) throws IOException {
		//File file = new File("images/logo01.jpg");
		//550x730
//		BufferedImage image = new BufferedImage(0, 0, 0);
//		new TextOnImage(image);
		
		// Create an image to save
		RenderedImage rendImage = myCreateImage();

		// Write generated image to a file
		try {
		    // Save as PNG
		    File file = new File("newimage.png");
		    ImageIO.write(rendImage, "png", file);

		    // Save as JPEG
		   // file = new File("newimage.jpg");
		    //ImageIO.write(rendImage, "jpg", file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

