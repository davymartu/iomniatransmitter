/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.utils;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.FileInputStream;
import java.io.IOException;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

public class GsWrapper {

	public static void main(String[] args) {
		try {
			StorageOptions.Builder optionsBuilder = StorageOptions.newBuilder();
			optionsBuilder.setProjectId("TestProject");
			Storage storage = optionsBuilder
					.setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("GoogleStorage/TestProject-aff1b1134823.json")))
					.build()
					.getService();

			// Create a bucket
			String bucketName = "Bucket"; // Change this to something unique
			storage.create(BucketInfo.of(bucketName));

			// Upload a blob to the newly created bucket
			BlobId blobId = BlobId.of(bucketName, "my_blob_name");
			BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("text/plain").build();
			storage.create(blobInfo, "a simple blob".getBytes(UTF_8));

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
