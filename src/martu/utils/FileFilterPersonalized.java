/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.utils;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

public class FileFilterPersonalized implements FileFilter {

	private List<String> extensions;

	public FileFilterPersonalized(String extension){
		this.extensions = Arrays.asList(extension.toLowerCase().split(" "));
	}

	public boolean accept(File pathname) {
		String ext = FilenameUtils.getExtension(pathname.getName()).toLowerCase();
		return extensions.contains(ext);
//		return  pathname.getName().toLowerCase().endsWith(extension) ;
	}
	
//	public static void main(String args[]) {
//		String acceptedExts = "jpg txt";
//		List<String> listExt= Arrays.asList(acceptedExts.toLowerCase().split(" "));
//		String ext = FilenameUtils.getExtension(new File("backup/test/P3809909.txt").getName());
//		System.out.println(listExt.contains(ext));
//	}
}
