/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.controller;

import java.io.File;
import java.net.ConnectException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import martu.domain.messaggi.GeneratoreNotifiche;
import martu.domain.messaggi.MessaggioSmtp;
import martu.domain.services.FtpConnector;
import martu.domain.services.SmtpMailManager;
import martu.utils.Configurator;
import martu.utils.FileFilterExtensionAndFirstChar;
import martu.utils.HistoryMemorizer;

public class SmtpModeStrategy implements Strategy {

	private final Logger logger = LogManager.getLogger(SmtpModeStrategy.class);
	private final DateFormat defaultDateFmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private final Calendar zeroCalendar = new GregorianCalendar();
	@Override
	public void execute() {

		logger.debug("Avvio Smtp task...");

		ArrayList<String> commas=new ArrayList<String>();

		for (char currentPrefixToProcess : Configurator.getInstance().getPropValue("job.prefix_to_process","IP").toCharArray()) {
			logger.debug("Adding comma :" +String.valueOf(currentPrefixToProcess));
			commas.add(String.valueOf(currentPrefixToProcess));
		}		

		try {

			if(Configurator.getInstance().getPropValue("ftp.in.enable","false").equals("true")) {
				FtpConnector fc1 = new FtpConnector();
				ArrayList<String> ext=new ArrayList<String>();
				ext.add("txt");

				if(Configurator.getInstance().getPropValue("ftp.in.debug","").equals("true")) {
					fc1.enableDebug();
				}

				fc1.connect(Configurator.getInstance().getPropValue("ftp.in.ip",""),
						Integer.parseInt(Configurator.getInstance().getPropValue("ftp.in.port","21")),
						Configurator.getInstance().getPropValue("ftp.in.username",""),
						Configurator.getInstance().getPropValue("ftp.in.password",""),
						Boolean.parseBoolean(Configurator.getInstance().getPropValue("ftp.in.pasv","true")));
				fc1.scaricaFile(Configurator.getInstance().getPropValue("ftp.in.folder",""),Configurator.getInstance().getPropValue("job.input_dir_path","txt"),commas,ext,true);

				if(!Configurator.getInstance().getPropValue("ftp.in.esiti_path","").isEmpty()) {
					fc1.appendNotifiche(Configurator.getInstance().getPropValue("ftp.in.esiti_path",""),Configurator.getInstance().getPropValue("job.esiti.file_path",""));
				}
				fc1.disconnetti();
			}
		} catch (ConnectException e) {
			logger.error(e.getMessage(),e);
			logger.error("errore di connessione al server FTP");			
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		
		boolean isConnectionPoolBuilded = false;
		try {
			File dir = new File(Configurator.getInstance().getPropValue("job.input_dir_path","txt"));
			File[] f = dir.listFiles(new FileFilterExtensionAndFirstChar("txt",commas));
			
			for(int i=0;i < f.length;i++){
				boolean toDelete = true;
				MessaggioSmtp mex = null;
				String tipo = f[i].getName().substring(0,1);
				if(!tipo.equals("F")) {
				
					if(!isConnectionPoolBuilded) {
						SmtpMailManager.getInstance().BuildConnectionPool();
						isConnectionPoolBuilded = true;
					}
					try {
						mex = new MessaggioSmtp(f[i],tipo);
						//					mex.getInput().close();
						//				} catch (AllegatoNotFoundException e) {
						if(mex.isAttachNotFound) {
							Calendar fileArrivalTime = Calendar.getInstance();					
							HistoryMemorizer.getInstance().addNewMessageWaiting(f[i].getName(), fileArrivalTime);
							Calendar fileArrivalTimeAfterSeconds = (Calendar) HistoryMemorizer.getInstance().getMessageWaitingAttachmentArrivalTime().get(f[i].getName()).clone();
							logger.debug("Arrival time is:" + defaultDateFmt.format( fileArrivalTimeAfterSeconds.getTime()));
							fileArrivalTimeAfterSeconds.add(Calendar.SECOND,Integer.parseInt(Configurator.getInstance().getPropValue("attachments.wait_attach_strategy_seconds","30")));
							logger.debug("Check if after of  " + defaultDateFmt.format(fileArrivalTimeAfterSeconds.getTime()));
							if(fileArrivalTime != null && fileArrivalTime.after(zeroCalendar) && !Calendar.getInstance().after(fileArrivalTimeAfterSeconds)) {
								toDelete = false;
								logger.debug("Wait for attachment ready...I'll try next time");
							}else {
								HistoryMemorizer.getInstance().getMessageWaitingAttachmentArrivalTime().remove(f[i].getName());
								logger.debug("Removed from waiting queue " + f[i].getName());

								SimpleDateFormat data_format=new SimpleDateFormat("dd/MM");
								String dataDB=data_format.format(new GregorianCalendar().getTime());
								SimpleDateFormat ora_format =new SimpleDateFormat("HH:mm:ss");
								String oraDB=ora_format.format(new GregorianCalendar().getTime());

								GeneratoreNotifiche.scriviNotificaLocale(dataDB, oraDB, mex.getNumeroRiferimento(),  mex.getTipo(), "VCH","1","0",mex.getCentroDiCosto(),mex.getDataOrigine());
							}
						}

					}catch (Exception e) {
						logger.error(e.getMessage(), e);
					}finally {
						if(mex != null && mex.getInput() != null)
							mex.getInput().close();

						if(toDelete) {
							logger.debug("Deleting file " + f[i].getName());
							f[i].delete();
						}
					}
				}
			}			
			
		} catch (ConnectException e) {
			logger.error(e.getMessage(),e);
			logger.error("errore di connessione al server FTP");			
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}finally {
			if(isConnectionPoolBuilded)
				SmtpMailManager.getInstance().CloseConnectionPool();			
		}
		logger.debug("Fine Smtp task...");
	}
}