/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import martu.utils.Configurator;

public class Controller {
	private static Controller instance;
	private Set<Context> allJob;
	private static final Logger logger = LogManager.getLogger(Controller.class);
	
	public static Controller getInstance(){
		if(instance==null)
			instance=new Controller();
		return instance;
	}
	
	public Controller() {
		this.allJob = new HashSet<Context>();
	}

	public void avvia(Strategy strategy){
		logger.info("Avvio " + strategy.getClass().getSimpleName());
		Context context = new Context(strategy,Integer.parseInt(Configurator.getInstance().getTimeout()));
		context.executeStrategy();
		allJob.add(context);
	}

	public void stop() {
		Iterator<Context> it = allJob.iterator();
		while(it.hasNext()) {
			it.next().fermaTimer();
		}		
	}

}
