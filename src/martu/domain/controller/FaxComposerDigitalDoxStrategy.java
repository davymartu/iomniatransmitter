/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.controller;

import java.io.File;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import martu.domain.messaggi.MessaggioFax;
import martu.utils.Configurator;
import martu.utils.FileFilterExtensionAndFirstChar;

public class FaxComposerDigitalDoxStrategy implements Strategy {

	private final Logger logger = LogManager.getLogger(FaxComposerDigitalDoxStrategy.class);

	@Override
	public void execute() {

		logger.debug("Avvio FaxComposerDigitalDoxStrategy task...");

		ArrayList<String> commas=new ArrayList<String>();
		commas.add("F");
		try {
			File dir = new File(Configurator.getInstance().getPropValue("job.input_dir_path","txt"));
			File[] f = dir.listFiles(new FileFilterExtensionAndFirstChar("txt",commas));

			for(int i=0;i < f.length;i++){
				boolean toDelete = true;
				String tipo = f[i].getName().substring(0,1);
				if(tipo.equals("F")) {
					MessaggioFax mex = null;
					try {
						mex = new MessaggioFax(f[i],tipo);
						mex.generaPdf();
						mex.buildCommandTxt();
						mex.buildZip();
					}catch (Exception e) {
						logger.error(e.getMessage(), e);
					}finally {
						if(mex != null && mex.getInput() != null)
							mex.getInput().close();
						if(toDelete) {
							logger.debug("Deleting file " + f[i].getName());
							f[i].delete();
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}


		logger.debug("Fine FaxComposerDigitalDoxStrategy task...");
	}
}
