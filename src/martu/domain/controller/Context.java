/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package martu.domain.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import martu.utils.Configurator;


public class Context {
	private final Logger logger = LogManager.getLogger(Context.class);
	private Strategy strategy;
	private Timer timer;
	private int seconds;
	private RemindTask rt;
	private String data;
	private SimpleDateFormat sdf;


	public Context(Strategy strategy,int seconds) {
		this.timer = new Timer();
		this.seconds=seconds;
		this.strategy = strategy;
	}

	public void executeStrategy() {
		sdf=new SimpleDateFormat("yyyy-MM-dd");
		data=sdf.format(new GregorianCalendar().getTime());
		rt=new RemindTask();
		timer.schedule(rt, 1,seconds*1000);
	}

	public void fermaTimer(){
		rt.cancel();
		timer.cancel();
		logger.info("Timer fermato...");
		System.exit(0);
	}

	public void ControlSwitchDate(){
		String dataAttuale=sdf.format(new GregorianCalendar().getTime());
		if (dataAttuale.equals(data)){
			return;
		}else{
			data=dataAttuale;
			if(Configurator.getInstance().getCloseAtMidnight()){
				logger.info("Impostata la chiusura notturna. Bye bye...");
				System.exit(0);
			}
		}
	}

	class RemindTask extends TimerTask {

		@Override
		public void run() {
			Calendar currentcal=Calendar.getInstance();
			ControlSwitchDate();
			if((currentcal.after(Configurator.getInstance().getInizioJob())&&
					currentcal.before(Configurator.getInstance().getFineJob()))){
				strategy.execute();
			}

		}

	}
}
