/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.controller;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import martu.domain.messaggi.AbstractMessaggio;
import martu.domain.messaggi.DocumentoDiViaggio;
import martu.domain.messaggi.DocumentoDiViaggioTxt;
import martu.domain.messaggi.DocumentoDiViaggioXml;
import martu.domain.services.DirectoryNonValidaException;
import martu.domain.services.FtpConnector;
import martu.domain.services.LoginNonValidaException;
import martu.utils.Configurator;
import martu.utils.FileFilterPersonalized;

public class GenericDocumentStrategy implements Strategy {

	private final Logger logger = LogManager.getLogger(GenericDocumentStrategy.class);
	private DocumentoDiViaggio messaggio;

	@Override
	public void execute() {

		logger.info("Avvio GenericDocumentStrategy...");
		FtpConnector ftpConnectorIn;
		File[] f;		
		ArrayList<String> companies = null;
		Iterator<String> companiesIterator; 
		try {
			StringTokenizer stringTokenizer = new StringTokenizer(Configurator.getInstance().getPropValue("document.companies",""), ";");
			companies=new ArrayList<String>();
			while (stringTokenizer.hasMoreElements()) {
				companies.add(stringTokenizer.nextToken());
			}
			companiesIterator = companies.iterator();

			ArrayList<String> ext=new ArrayList<String>();
			ext.add("txt");
			ext.add("xml");


			if(Configurator.getInstance().getPropValue("document.ftp.in.enable","false").equals("true")) {
				ftpConnectorIn = new FtpConnector();

				if(Configurator.getInstance().getPropValue("document.ftp.in.debug","").equals("true")) {
					ftpConnectorIn.enableDebug();
				}

				ftpConnectorIn.connect(Configurator.getInstance().getPropValue("document.ftp.in.ip",""),
						Integer.parseInt(Configurator.getInstance().getPropValue("document.ftp.in.port","21")),
						Configurator.getInstance().getPropValue("document.ftp.in.username",""),
						Configurator.getInstance().getPropValue("document.ftp.in.password",""),
						Boolean.parseBoolean(Configurator.getInstance().getPropValue("document.ftp.in.pasv","true")));


				while (companiesIterator.hasNext()) {
					String currentCompany = companiesIterator.next();
					ftpConnectorIn.scaricaFile(Configurator.getInstance().getPropValue("document.ftp.in.folder." + currentCompany,""),
							Configurator.getInstance().getPropValue("document.work_folder."+ currentCompany,"documents"),null,ext,true);	   
				}
				ftpConnectorIn.disconnetti();
			}

		} catch (ConnectException e) {
			logger.error(e);
			logger.error("errore di connessione al server FTP");
		} catch (IOException e) {
			logger.error(e);
		} catch (DirectoryNonValidaException e) {
			logger.error(e);
		} catch (LoginNonValidaException e) {
			logger.error(e);
		} 
		FtpConnector ftpConnectorOut = null;
		try{
			companiesIterator = companies.iterator();
			while (companiesIterator.hasNext()) {
				String currentCompany = companiesIterator.next();
				//Leggo i file txt nella cartella
				File dir = new File(Configurator.getInstance().getPropValue("document.work_folder."+ currentCompany,"documents")); 
				f = dir.listFiles(new FileFilterPersonalized("txt xml"));
				
				if(f.length != 0){

					if(Configurator.getInstance().getPropValue("document.ftp.out.enable","false").equals("true")) {
						ftpConnectorOut = new FtpConnector();

						if(Configurator.getInstance().getPropValue("document.ftp.out.debug","").equals("true")) {
							ftpConnectorOut.enableDebug();
						}

						ftpConnectorOut.connect(Configurator.getInstance().getPropValue("document.ftp.out.ip",""),
								Integer.parseInt(Configurator.getInstance().getPropValue("document.ftp.out.port","21")),
								Configurator.getInstance().getPropValue("document.ftp.out.username",""),
								Configurator.getInstance().getPropValue("document.ftp.out.password",""),
								Boolean.parseBoolean(Configurator.getInstance().getPropValue("document.ftp.out.pasv","true")));

					}

					for(int i=0;i<f.length;i++){
						try{
							
							switch(FilenameUtils.getExtension(f[i].getName().toLowerCase())) {
							case "txt":
								messaggio = new DocumentoDiViaggioTxt(f[i],currentCompany);
								((AbstractMessaggio)messaggio).getInput().close();
								break;
							case "xml":
								messaggio = new DocumentoDiViaggioXml(f[i],currentCompany);
								break;
							}

							if(Configurator.getInstance().getPropValue("document.ftp.out.enable","false").equals("true")) {
								ftpConnectorOut.cambiaDirectory(Configurator.getInstance().getPropValue("document.ftp.out.folder",""));
								ftpConnectorOut.creaDirectory(messaggio.getFirstDirToCreate());
								ftpConnectorOut.cambiaDirectory(messaggio.getFirstDirToCreate());
								if(messaggio.isSecondDir()){
									ftpConnectorOut.creaDirectory(messaggio.getSecondDirToCreate());
									ftpConnectorOut.cambiaDirectory(messaggio.getSecondDirToCreate());
								}
								if(ftpConnectorOut.caricaFileSingolo(messaggio.getPercorsoPdfGenerato(),true))
									f[i].delete();
							}else {
								if(f[i].delete())
									logger.info(f[i].getName() + " deleted");
								else
									logger.warn(f[i].getName() + " not deleted");
							}

						}catch(NullPointerException e){
							logger.error(e.getMessage(),e);
						}catch(IOException e){
							logger.error(e.getMessage(),e);
						} catch (ParserConfigurationException e) {
							logger.error(e.getMessage(),e);
						} catch (SAXException e) {
							logger.error(e.getMessage(),e);
						} catch (TransformerException e) {
							logger.error(e.getMessage(),e);
						}
					}
					if(Configurator.getInstance().getPropValue("document.ftp.out.enable","false").equals("true")) {
						ftpConnectorOut.disconnetti();	
					}			
				}
			}
		} catch (ConnectException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DirectoryNonValidaException e) {
			e.printStackTrace();
		} catch (LoginNonValidaException e) {
			e.printStackTrace();
		} 
	}
}
