/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.services;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.sun.mail.util.MailSSLSocketFactory;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import martu.domain.messaggi.AllegatoNotFoundException;
import martu.domain.messaggi.MailConstructException;
import martu.domain.messaggi.MessaggioSmtp;
import martu.domain.messaggi.MtsMail;
import martu.utils.Configurator;
import martu.utils.CreaCartelle;
import martu.utils.VariousUtils;

public class SmtpMailManager {

	private final Logger logger = LogManager.getLogger(SmtpMailManager.class);

	private static SmtpMailManager instance;
	private CreaCartelle creatorBackupSaveEml;
	private HashMap<String,SmtpServerSession> connectionPool;
	public static SmtpMailManager getInstance(){
		if(instance==null)
			instance=new SmtpMailManager();
		return instance;
	}

	public SmtpMailManager() {

	}

	public void BuildConnectionPool() {
		logger.debug("building connection pool...");
		connectionPool = new HashMap<String,SmtpServerSession>();
		//Se le companies sono pi� di una e la prima � diversa da "default" uso il codice reparto/centro di costo come configurazione SMTP
		if(Configurator.getInstance().getSMTPCompanies().size() > 0 ) { //&& Configurator.getInstance().getSMTPCompanies().get(0).toLowerCase().equals("default")
			logger.debug("searching company...");
			Iterator<String> itCompanies = Configurator.getInstance().getSMTPCompanies().iterator();
			while(itCompanies.hasNext()) {
				String currentCompany = itCompanies.next().toLowerCase();

				Properties props =  new Properties();   
				props.put("mail.smtp.host", Configurator.getInstance().getPropValue("smtp.host." + currentCompany,""));
				props.put("mail.smtp.port",  Configurator.getInstance().getPropValue("smtp.port." + currentCompany,"25"));

				Session session;
				if(  !Configurator.getInstance().getPropValue("smtp.user." + currentCompany,"").isEmpty() 
						&& !Configurator.getInstance().getPropValue("smtp.password." + currentCompany,"").isEmpty()){
					props.put("mail.smtp.auth","true");			
					Authenticator auth = new SMTPAuthenticator(Configurator.getInstance().getPropValue("smtp.user." + currentCompany,""),
							Configurator.getInstance().getPropValue("smtp.password." + currentCompany,""));	

					if(Boolean.parseBoolean(Configurator.getInstance().getPropValue("smtp.ssl." + currentCompany,"false"))) {
						MailSSLSocketFactory sf;
						try {
							sf = new MailSSLSocketFactory();
							sf.setTrustAllHosts(true);
							props.put("mail.smtp.ssl.enable", "true");
							props.put("mail.smtp.ssl.trust", "*");
							props.put("mail.smtp.ssl.socketFactory", sf);
						} catch (GeneralSecurityException e) {
							logger.warn(e.getMessage());
						}
					}

					logger.info("Connecting to SMTP with authenticator...");
					session = Session.getInstance(props, auth);
				}else{
					logger.info("Connecting to SMTP...");
					session = Session.getInstance(props);
				}

				if(Configurator.getInstance().getPropValue("smtp.debug","").equals("true")) {

					PrintStream loggerPS = IoBuilder.forLogger(SmtpMailManager.class)
							.setLevel(Level.DEBUG)
							.buildPrintStream();
					session.setDebugOut(loggerPS);
					session.setDebug(true);
					logger.debug("debug SMTP is enabled...");
				}

				logger.debug("adding to connection pool " + currentCompany);
				try {
					Transport currentTransport = session.getTransport("smtp");
					currentTransport.connect();
					SmtpServerSession currentSmtpServerSession = new SmtpServerSession(session,currentTransport);
					connectionPool.put(currentCompany, currentSmtpServerSession);
				} catch (NoSuchProviderException e) {
					logger.warn(e.getMessage());
				} catch (MessagingException e) {
					logger.warn(e.getMessage());
				}				
			}
		}
	}

	public void CloseConnectionPool() {
		logger.debug("closing connection pool");
		Iterator<String> smtpConnectionKeyIt = connectionPool.keySet().iterator();
		while(smtpConnectionKeyIt.hasNext()) {
			String smtpConnectionKey = smtpConnectionKeyIt.next();
			SmtpServerSession currentSmtpServerSession = connectionPool.get(smtpConnectionKey);
			try {
				currentSmtpServerSession.getTransport().close();
				logger.debug("SMTP connection to " + smtpConnectionKey + " closed!");
			} catch (NoSuchProviderException e) {
				logger.warn(e.getMessage(), e);
			} catch (MessagingException e) {
				logger.warn(e.getMessage(), e);
			}
		}
	}

	public String leggiTemplate(String data,String protocollo,String utente,String mittente){
		String linea,comodo="";
		String crlf = System.getProperty( "line.separator" );
		try {
			BufferedReader input = new BufferedReader(new FileReader("config/template.txt"));
			for(linea = input.readLine(); linea!=null; linea = input.readLine())
			{
				linea=linea.replaceAll("&DATE",data);
				linea=linea.replaceAll("&PROT",protocollo);
				linea=linea.replaceAll("&USER",utente);
				linea=linea.replaceAll("&MITT",mittente);
				comodo+= linea + crlf;
			}
			input.close();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return comodo;

	}

	public MtsMail leggiTemplateHTML(MessaggioSmtp mex) throws Exception {
		String linea,comodo="",
				pathOfTemplate= mex.getCentroDiCosto() + "/" + mex.getMailParams().get("template") + ".html";		
		String crlf = System.getProperty( "line.separator" );
		try {
			logger.debug("Using template " + pathOfTemplate);
			Reader reader =new InputStreamReader(new FileInputStream("config/templates/" + pathOfTemplate ), StandardCharsets.UTF_8);
			BufferedReader input = new BufferedReader(reader);

			for(linea = input.readLine(); linea!=null; linea = input.readLine())
			{
				if(linea.contains("$$")) {
					Pattern currentPattern = Pattern.compile("\\$\\$(\\w+?)\\$\\$");
					Matcher m = currentPattern.matcher(linea);
					while (m.find()) {
						String keyFounded = m.group(1);
						logger.debug("Found key:" + keyFounded);
						linea = linea.replace("$$"+ keyFounded + "$$", mex.getMailParams().get(keyFounded.toLowerCase()));
					}
				}
				comodo+= linea + crlf;
			}
			input.close();

			Document doc = Jsoup.parse(comodo);
			String title =  doc.title();

			if(title.contains("$$")) {
				logger.debug("Title contains special chars..");
				Pattern currentPattern = Pattern.compile("\\$\\$(\\w+?)\\$\\$");
				Matcher m = currentPattern.matcher(title);
				while (m.find()) {
					String keyFounded = m.group(1);
					logger.debug("Found key on title:" + keyFounded);
					linea = linea.replace("$$"+ keyFounded + "$$", mex.getMailParams().get(keyFounded.toLowerCase()));
				}
			}
			return new MtsMail(title,comodo);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			throw new Exception(e);
		}	
	}

	public String leggiStandardTemplateHTML(String testo, String data,String protocollo,String utente,String mittente,String logo,String company) throws TemplateNonTrovato{
		String linea,comodo="";
		String crlf = System.getProperty( "line.separator" );
		String pathOfStandardTemplate= "config/template.html";
		try {

			if(Configurator.getInstance().getSMTPCompanies().size() > 1) {
				pathOfStandardTemplate = "config/templates/" + company.toLowerCase() +"/template.html";
			}

			if(Configurator.getInstance().getPropValue("smtp.template.discriminator","").equalsIgnoreCase("LOGO")) {
				pathOfStandardTemplate = "config/templates/" + logo + "/template.html";
			}

			if(!new File(pathOfStandardTemplate).exists()) {
				logger.warn("template doesn't exists, setting default!");
				pathOfStandardTemplate= "config/template.html";
			}

			logger.debug("reading standard template from: " + pathOfStandardTemplate);
			Reader reader =new InputStreamReader(new FileInputStream(pathOfStandardTemplate), StandardCharsets.UTF_8);
			BufferedReader input = new BufferedReader(reader);
			testo = testo.replaceAll("(\r\n|\n)", "<br/>");
			for(linea = input.readLine(); linea!=null; linea = input.readLine())
			{
				linea=linea.replace("&LOGO",logo);
				linea=linea.replace("&DATE",data);
				linea=linea.replace("&PROT",protocollo);
				linea=linea.replace("&USER",utente);
				linea=linea.replace("&MITT",mittente);
				linea=linea.replace("<!--##Testo###-->",testo);
				comodo+= linea + crlf;
			}
			input.close();
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			throw new TemplateNonTrovato(pathOfStandardTemplate + " non trovato!");
		}
		return comodo;
	}

	public void send(MessaggioSmtp mex)throws MessagingException,AddressException, GeneralSecurityException, MailConstructException, AllegatoNotFoundException, TemplateNonTrovato{
		logger.info("Preparing email...");
		logger.info("Getting SMTP connection from pool :" +  mex.getCompany().toLowerCase());
		//Session currentSession = connectionPool.get(mex.getCompany().toLowerCase());
		SmtpServerSession currentSmtpServerSession = connectionPool.get(mex.getCompany().toLowerCase());
		MimeMessage message = new MimeMessage(currentSmtpServerSession.getSession());
		InternetAddress ia=new InternetAddress();

		if(!mex.getMittente().trim().isEmpty() && 
				!Configurator.getInstance().getPropValue("smtp.default_sender_only." + mex.getCompany(),"false").equals("true")) {		
			ia.setAddress(mex.getMittente());
		}else {
			ia.setAddress(Configurator.getInstance().getPropValue("smtp.default_sender." + mex.getCompany(),""));
		}

		if(!mex.getNomeMittente().isEmpty()) {			
			try {
				ia.setPersonal(Configurator.getInstance().getPropValue("smtp.default_sender_name." + mex.getCompany(),"") + " - " + mex.getNomeMittente());
			} catch (UnsupportedEncodingException e) {
				logger.error(e.getMessage(), e);
			}
		}else {
			try {
				ia.setPersonal(Configurator.getInstance().getPropValue("smtp.default_sender_name." + mex.getCompany(),""));
			} catch (UnsupportedEncodingException e) {
				logger.error(e.getMessage(), e);
			}
		}
		logger.debug("Sender name is '" + ia.getPersonal() + "'");
		if(Configurator.getInstance().getPropValue("smtp.use_reply_to_strategy." + mex.getCompany(),"").equals("true"))
		{
			InternetAddress addr1=new InternetAddress();
			addr1.setAddress(Configurator.getInstance().getPropValue("smtp.reply_to_address." + mex.getCompany(),""));
			//message.setFrom(addr1);
			message.setReplyTo(new Address[]{ia});
		}

		logger.info("Mittente messaggio:"+ ia.getPersonal() + " <" + ia.getAddress()+ ">");
		message.setFrom(ia);

		//DM For testing purpose
		if(!Configurator.getInstance().getPropValue("smtp.test_mail_recipient","").isEmpty()) {
			logger.debug("Setting mail recipient TEST to :" + Configurator.getInstance().getPropValue("smtp.test_mail_recipient",""));
			mex.setDestinatario(Configurator.getInstance().getPropValue("smtp.test_mail_recipient",""));
		}

		if(IsPermittedDomain(mex.getDestinatario()))
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(mex.getDestinatario()));

		if(!Configurator.getInstance().getPropValue("smtp.cc_mail_recipient","").isEmpty()) {
			String ccRecipient = Configurator.getInstance().getPropValue("smtp.cc_mail_recipient","");
			StringTokenizer st = new StringTokenizer(ccRecipient, ";");
			while(st.hasMoreTokens()) {
				ccRecipient = st.nextToken();
				logger.debug("Setting mail recipient CC to: " +  ccRecipient);
				if(IsPermittedDomain(ccRecipient))
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(ccRecipient));	
			}			
		}

		if(!mex.getCcMailRecipients().isEmpty()) {
			String ccRecipient = mex.getCcMailRecipients();
			StringTokenizer st = new StringTokenizer(ccRecipient, ";");
			while(st.hasMoreTokens()) {
				ccRecipient = st.nextToken();
				logger.debug("Setting mail recipient CC from TXT to: " +  ccRecipient);
				if(IsPermittedDomain(ccRecipient))
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(ccRecipient));	
			}		
		}

		if(!Configurator.getInstance().getPropValue("smtp.ccn_mail_recipient","").isEmpty()) {
			String bccRecipient = Configurator.getInstance().getPropValue("smtp.ccn_mail_recipient","");
			StringTokenizer st = new StringTokenizer(bccRecipient, ";");
			while(st.hasMoreTokens()) {
				bccRecipient = st.nextToken();
				logger.debug("Setting mail recipient BCC to: " + bccRecipient );
				if(IsPermittedDomain(bccRecipient))
					message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccRecipient));	
			}
		}
		message.setSubject(mex.getOggetto());

		MimeBodyPart messageBodyPart = new MimeBodyPart();
		Multipart mpMixed = new MimeMultipart("mixed");
		String emailBodyText = "";

		if( mex.testoDellaMail == null || mex.testoDellaMail.equals("") || 
				(mex.getFileSourceTXT().getName().toUpperCase().startsWith("P") && mex.isAllegatoTestoActive) 
				|| 	((mex.getFileSourceTXT().getName().toUpperCase().startsWith("P") || mex.getFileSourceTXT().getName().toUpperCase().startsWith("E"))
						&& Configurator.getInstance().getPropValue("emailbody.enable_template_txt","false").equals("true"))){
			emailBodyText = leggiTemplate(mex.getData(),mex.getNumeroRiferimento(),mex.getUtente(),mex.getMittente());
		}else {
			emailBodyText = mex.testoDellaMail;
		}

		//PLAIN TEXT
		Multipart mpMixedAlternative = newChild(mpMixed, "alternative");
		MimeBodyPart textPart = new MimeBodyPart();
		textPart.setContent(emailBodyText,"text/plain; charset=UTF-8");
		mpMixedAlternative.addBodyPart(textPart);
		if(new File("config/template.html").exists() || mex.getMailParams().containsKey("template")){
			try {
				// HTML TEXT
				Multipart mpRelated = newChild(mpMixedAlternative,"related");
				MimeBodyPart  htmlPart = new MimeBodyPart();
				if(mex.getMailParams().containsKey("template")) {
					MtsMail mtsMail= leggiTemplateHTML(mex);
					htmlPart.setContent(mtsMail.getHtmlBody(),"text/html; charset=UTF-8");
					if (!mtsMail.getSubject().isEmpty()) {
						message.setSubject(mtsMail.getSubject(),"utf-8");
					}
				} else {
					//getCentroDiCosto() -> getCompany()
					htmlPart.setContent(leggiStandardTemplateHTML(emailBodyText,mex.getData(),mex.getNumeroRiferimento(),mex.getUtente(),mex.getMittente(),mex.getLogo(),mex.getCompany()), "text/html; charset=UTF-8");
				}
				mpRelated.addBodyPart(htmlPart);

				//messageBodyPart.setText(new String(Files.readAllBytes(Paths.get("templateHTML.txt")),StandardCharsets.ISO_8859_1), "ISO_8859_1", "html");
				if(Configurator.getInstance().getPropValue("emailbody.logo_image_embedded","false").equals("true")) {					
					String logoImage = "images/" +  Configurator.getInstance().getPropValue("emailbody." + mex.getLogo().toLowerCase(),"logo01.jpg");

					if(!new File(logoImage).exists()){
						logger.debug(logoImage + " doesn't exist, adding default");
						logoImage = "images/" +  Configurator.getInstance().getPropValue("emailbody.logo_default","logo01.jpg");
					}
					logger.debug("adding " + logoImage + " with cid:<logo>");

					MimeBodyPart imagePart = new MimeBodyPart();
					DataSource fds = new FileDataSource(logoImage);
					imagePart.setDataHandler(new DataHandler(fds));
					imagePart.setHeader("Content-ID", "<logo>");
					//						         imagePart.attachFile(logoImage);
					//						         imagePart.setContentID("<logo>");
					//						imagePart.setDisposition(MimeBodyPart.INLINE);
					mpRelated.addBodyPart(imagePart);
					logger.debug("added image to body!");
				}
				//					
			}catch(TemplateNonTrovato e) {
				throw new TemplateNonTrovato(e.getMessage());
			}catch(Exception e){
				logger.error(e.getMessage(),e);
				throw new MailConstructException(e.getMessage());				
			}
		}

		if(mex.percorsoPdfGenerato != null) {
			messageBodyPart=new MimeBodyPart();
			String pdfFileName = new File(mex.percorsoPdfGenerato).getName();			
			DataSource source = new FileDataSource(mex.percorsoPdfGenerato);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(pdfFileName);			
			mpMixed.addBodyPart(messageBodyPart);
			logger.debug("Attachment attach '" +  pdfFileName  + "' added");
		}

		if(mex.isAllegatoTestoActive) {
			try {
				messageBodyPart=new MimeBodyPart();
				DataSource ds = new ByteArrayDataSource(mex.testoDellaMail, "text/plain");
				messageBodyPart.setDataHandler(new DataHandler(ds));
				messageBodyPart.setFileName(mex.getFileSourceTXT().getName());
				mpMixed.addBodyPart(messageBodyPart);
				logger.debug("Attachment ALLEGATESTO with name '" + mex.getFileSourceTXT().getName()  +"' added");
			}catch(Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

		logger.debug("number of attachments is  '" + mex.getAllegati().size() + "'");
		for(int i=0;i< mex.getAllegati().size();i++){
			logger.debug("adding attach '" + mex.getAllegati().get(i) + "'");
			messageBodyPart=new MimeBodyPart();
			if(!mex.getAllegati().get(i).isEmpty()) {
				if( Configurator.getInstance().getPropValue("attachments.share_enable","false").equals("true")){
					String host = Configurator.getInstance().getPropValue("attachments.share_path","");
					String user = Configurator.getInstance().getPropValue("attachments.share_username","");
					String password =Configurator.getInstance().getPropValue("attachments.share_password","");
					String domain =Configurator.getInstance().getPropValue("attachments.share_domain","");
					logger.info("processing '" +  mex.getAllegati().get(i) + "'");

					String attachPath = VariousUtils.transformAttachPath(mex.getAllegati().get(i));
					attachPath="smb://"+ host.replaceAll("\\\\\\\\", "").replaceAll("\\\\", "/")  + attachPath;

					logger.info("Connecting to '" + attachPath + "'");
					try {
						NtlmPasswordAuthentication authSmb;
						if(!user.isEmpty() && !user.isEmpty()) {
							logger.info("using NTLM authentication with user '" + domain +"\\" + user + "'");
							authSmb = new NtlmPasswordAuthentication(domain,user, password);						
						}else {
							logger.info("using anonymous authentication..");
							authSmb = NtlmPasswordAuthentication.ANONYMOUS;
						}
						SmbFile smbDestDirPath= new SmbFile(attachPath,authSmb);	
						if(smbDestDirPath.exists()) {
							SmbFileInputStream SmbFileInputStream = new SmbFileInputStream(smbDestDirPath);
							DataSource ds = new ByteArrayDataSource(SmbFileInputStream, "application/octet-stream");
							messageBodyPart.setDataHandler(new DataHandler(ds));
							messageBodyPart.setFileName(smbDestDirPath.getName());
							mpMixed.addBodyPart(messageBodyPart);
							logger.info("Attachment added");
						}else {
							logger.warn("Attachment not found");
						}
					} catch (MalformedURLException e) {
						logger.error(e.getMessage(),e);
					} catch (SmbException e) {
						logger.error(e.getMessage(),e);
						//Recupera da folder locale di recovery
						if(Configurator.getInstance().getPropValue("attachments.recovery.enable","false").equals("true")) {
							logger.info("Attachment recovery folder is active! Checking for recovery attachments folder...");
							attachPath =  Configurator.getInstance().getPropValue("attachments.recovery.local_folder",".") + File.separator + mex.getAllegati().get(i);
							addLocalAttach(messageBodyPart, mpMixed, attachPath);
						}
					} catch (IOException e) {
						logger.error(e.getMessage(),e);
					}
				}else {
					String attachPath =   VariousUtils.transformAttachPath(mex.getAllegati().get(i));
					addLocalAttach(messageBodyPart, mpMixed, attachPath);
				}
			}			
		}

		message.setContent(mpMixed);
		message.saveChanges();
		if( Configurator.getInstance().getPropValue("smtp.save_eml","false").equals("true")) {
			try {
				creatorBackupSaveEml =new CreaCartelle("txt/backup/eml");
				String nomeFileDiBackup = creatorBackupSaveEml.getGeneratedDirPath() +
						mex.getNomeFax()
				+ ".eml";

				OutputStream out = new FileOutputStream(nomeFileDiBackup); 
				message.writeTo(out);
				logger.info("Saved eml to " + nomeFileDiBackup);
			}catch(Exception e) {
				logger.error(e.getMessage(),e);
			}
		}
		currentSmtpServerSession.getTransport().sendMessage(message, message.getAllRecipients());
		//		Transport.send(message);
		//		Transport transport = session.getTransport("smtp");
		//		transport.send(message);
		//		transport.close();
		logger.info("Messaggio inviato, file:"+ mex.getNomeFax());
	}

	private void addLocalAttach(MimeBodyPart messageBodyPart, Multipart mpMixed, String attachPath)
			throws MessagingException, AllegatoNotFoundException {
		logger.info("sarching file " + attachPath);							
		File attachFile= new File(attachPath);	
		if(attachFile.exists()) {
			FileInputStream fileInputStream;
			try {
				fileInputStream = new FileInputStream(attachFile);
				DataSource ds = new ByteArrayDataSource(fileInputStream, "application/octet-stream");
				messageBodyPart.setDataHandler(new DataHandler(ds));
				messageBodyPart.setFileName(attachFile.getName());
				mpMixed.addBodyPart(messageBodyPart);
				logger.info("Attachment added");
			} catch (FileNotFoundException e) {
				logger.error("File not found!",e);							
			} catch (IOException e) {
				logger.error("File read error!",e);
			}
		}else {
			logger.warn("Attachment not found");
			if(Configurator.getInstance().getPropValue("attachments.wait_attach_strategy","false").equals("true")) {
				throw new AllegatoNotFoundException("Attach '" + attachFile.getAbsolutePath() + " not available, retry next loop");
			}
		}
	}


	private boolean IsPermittedDomain(String destinatario) {
		String permittedDomains = Configurator.getInstance().getPropValue("smtp.permitted_domains_recipient","*");
		if(permittedDomains.equals("*"))
			return true;

		StringTokenizer st = new StringTokenizer(permittedDomains, ";");
		String currentDomain;
		while(st.hasMoreTokens()) {
			currentDomain = st.nextToken();
			logger.debug("current domain to check is:" +  currentDomain);
			if(destinatario.trim().toLowerCase().endsWith(currentDomain.trim().toLowerCase())) {
				logger.debug(destinatario + " domain is permitted");
				return true;
			}
		}	
		logger.debug(destinatario + " is not permitted!");
		return false;
	}


	private class  SMTPAuthenticator  extends Authenticator {
		private String user;
		private String password;

		public SMTPAuthenticator(String user,String password) {
			this.user = user;
			this.password = password;
		}
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(this.user, this.password);
		}
	}

	private Multipart newChild(Multipart parent, String alternative) throws MessagingException {
		MimeMultipart child =  new MimeMultipart(alternative);
		final MimeBodyPart mbp = new MimeBodyPart();
		parent.addBodyPart(mbp);
		mbp.setContent(child);
		return child;
	}

	public static void main(String args[]) throws KeyManagementException, NoSuchAlgorithmException, IOException, CertificateEncodingException {
		String path= "\\\\192.168.1.1\\pippo";
		System.out.println(path);
		System.out.println(path.replaceAll("\\\\\\\\", ""));
		//		String linea,testo="Ciao\r\n" + 
		//				"Queste � una mail che voglio far$ti leggere\r\n" + 
		//				"Cordiali Saluti\r\n" + 
		//				"\r\n" + 
		//				"Ti ci metto anche un po' di caratteri speciali:!\"�%&/()=?^�|�� "
		//		,comodo="";
		//		
		//		String crlf = System.getProperty( "line.separator" );
		//		try {
		//			//BufferedReader input = new BufferedReader(new FileReader("email/templateHTML.txt"));
		//			Reader reader =new InputStreamReader(new FileInputStream("config/template.html"), StandardCharsets.UTF_8);
		//			BufferedReader input = new BufferedReader(reader);
		//			testo = testo.replaceAll("(\r\n|\n)", "<br/>");
		//			for(linea = input.readLine(); linea!=null; linea = input.readLine())
		//			{
		//				
		//				linea=linea.replace("<!--##Testo###-->",testo);
		//				comodo+= linea + crlf;
		//			}
		//			input.close();
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}
		//		
		//		System.out.println(comodo);
		//		URL url = new URL("smtp://smtps.aruba.it:465");
		//
		//		SSLContext sslCtx = SSLContext.getInstance("TLS");
		//		sslCtx.init(null, new TrustManager[]{ new X509TrustManager() {
		//
		//			private X509Certificate[] accepted;
		//
		//			@Override
		//			public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
		//			}
		//
		//			@Override
		//			public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
		//				accepted = xcs;
		//			}
		//
		//			@Override
		//			public X509Certificate[] getAcceptedIssuers() {
		//				return accepted;
		//			}
		//		}}, null);
		//
		//		HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
		//
		//		connection.setHostnameVerifier(new HostnameVerifier() {
		//
		//			@Override
		//			public boolean verify(String string, SSLSession ssls) {
		//				return true;
		//			}
		//		});
		//
		//		connection.setSSLSocketFactory(sslCtx.getSocketFactory());
		//
		//		if (connection.getResponseCode() == 200) {
		//			Certificate[] certificates = connection.getServerCertificates();
		//			for (int i = 0; i < certificates.length; i++) {
		//				Certificate certificate = certificates[i];
		//				File file = new File("C:\\users\\davym\\Desktop\\" + i + ".crt");
		//				byte[] buf = certificate.getEncoded();
		//
		//				FileOutputStream os = new FileOutputStream(file);
		//				os.write(buf);
		//				os.close();
		//			}
		//		}
		//
		//		connection.disconnect();
	}

}