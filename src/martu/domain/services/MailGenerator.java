/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.services;

import java.io.PrintStream;

import martu.domain.messaggi.AbstractMessaggio;

public class MailGenerator{
	
	private PrintStream out;
	private AbstractMessaggio messaggio;
	public MailGenerator(PrintStream out,AbstractMessaggio messaggio) {
		super();
		this.out = out;
		this.messaggio=messaggio;
	}
	@SuppressWarnings("unused")
	private void scriviRigaSostituendoSpazi(String string) {
		String linea= string.replaceAll(" ", "&nbsp;");
		out.println(linea);
	}
	@SuppressWarnings("unused")
	private void scriviFooterEmail() {
		scriviRiga("</FONT><BR/>");
		scriviRiga("</body> </html>");
	}

	
	@SuppressWarnings("unused")
	private void scriviHeaderEmail(){
		scriviRiga("MIME-Version: 1.0");
		scriviRiga("Content-Type: text/html; charset=iso-8859-1");
		scriviRiga("From: \""+ messaggio.getUtente() +"\" "+ "<"+ messaggio.getMittente()+ ">");
		scriviRiga("Subject: " + messaggio.getOggetto());
		scriviRiga("");
		scriviRiga("<html>");
		scriviRiga("<head></head>");
		scriviRiga("<body>");
		scriviRiga("<table border=\"0\" cellpadding=\"5\" width=\"600\" align=\"left\" height=\"80\">" +
				"<tbody><tr>" +
				"<td valign=\"top\" width=\"300\" align=\"left\"><strong>Un Altro Sole<br />" +
				" di New   Challenge S.r.l.<br /> </strong>Via Ripamonti, 66 - 20141" +
				"   MILANO<br /> Tel +39 02/5731321<br />" +
				"   Fax +39 02/57313226<br />  email " +
				"<a title=\"blocked::mailto:info@unaltrosole.com\" href=\"mailto:info@unaltrosole.com\">info@unaltrosole.com</a>" +
				"<br /> internet <a title=\"blocked::http://www.unaltrosole.com/\" " +
				"href=\"http://www.unaltrosole.com\">www.unaltrosole.com</a><br /></td>" +
				"   <td valign=\"center\" width=\"300\" align=\"left\"><br />    <br />" +
				"        CCIAA C.F. E P.IVA   04369540960<br />  NR ISCRIZIONE REA MILANO 1743180<br />" +
				"       CAP SOCIALE i.v. 310.000,00   euro<br />      <br /></td>" +
		"   </tr>  </tbody></table>");
		scriviRiga("<br/><br/><br/><br/><br/><br/><br/><br/><HR align=left width=600><br/><br/><FONT size=-2 face=Courier>");

	}
	protected void scriviRiga(String linea){
		if(out!=null)
			out.println(linea);
	}
	protected void scriviRigaVuota(){
		out.println();
	}
	

}
