/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.services;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;


public class Statistiche extends java.util.Observable {
	private long notificheProcessate;
	private long messaggiInErrore;
	private String dataAttuale;
	private int MessaggiProcessati;
	private static Statistiche instance;
	
	public static Statistiche getInstance(){
		if(instance==null)
			instance=new Statistiche();
		return instance;
		
	}
	
	public void addNotificaProcessata(){
		notificheProcessate++;
		this.setChanged();
		this.notifyObservers();
	}
	
	public long getNotificheProcessate(){
		return notificheProcessate;
	}
	
	public void addMessaggioInErrore(){
		messaggiInErrore++;
	}
	
	public long getMessaggiProcessati(){
		return MessaggiProcessati;
	}
	
	public void addMessaggiProcessati(){
		MessaggiProcessati++;
		this.setChanged();
		this.notifyObservers();
	}
	
	public long getMessaggiInErrore(){
		return messaggiInErrore;
	}
	
	public String getDataAttuale() {
		return dataAttuale;
	}
	
	public Statistiche(){		
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy 'alle' HH:mm");
		dataAttuale=sdf.format(new GregorianCalendar().getTime());
	}
}
