package martu.domain.messaggi;

public interface DocumentoDiViaggio {
	
	boolean isSecondDir();
	
	String getFirstDirToCreate();

	String getSecondDirToCreate();

	void tokenizerDir(String percorso);
	
	String getPercorsoPdfGenerato();
}
