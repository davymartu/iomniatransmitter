/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.messaggi;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import martu.utils.Configurator;
import martu.utils.Copier;
import martu.utils.CreaCartelle;
import martu.utils.VariousUtils;


public class MessaggioFax extends AbstractMessaggio {
	private final Logger logger = LogManager.getLogger(MessaggioFax.class);

	public boolean isAttachNotFound=false;
	protected CreaCartelle creatorBackupOutZip;

	public MessaggioFax(File file,String tipoPrefix) throws IOException{
		super(file);		
		setPrimaRiga(input.readLine());
		creatorBackupOutZip=new CreaCartelle("txt/backup/zip");
	}

	public void buildZip() throws IOException {
		List<String> srcFiles = Arrays.asList(percorsoOutput + this.getNomeFax() + ".txt", this.percorsoPdfGenerato);
		logger.debug("number of attachments is  '" + this.getAllegati().size() + "'");
		File zipOutFile = new File (percorsoOutput + this.getNomeFax() + ".zip");
		FileOutputStream fos = new FileOutputStream(zipOutFile);
		ZipOutputStream zipOut = new ZipOutputStream(fos);

		for(int i=0;i< this.getAllegati().size();i++){
			logger.debug("adding attach '" + this.getAllegati().get(i) + "'");			

			if(!this.getAllegati().get(i).isEmpty()) {
				if(Configurator.getInstance().getPropValue("attachments.share_enable","false").equals("true")){
					String host = Configurator.getInstance().getPropValue("attachments.share_path","");
					String user = Configurator.getInstance().getPropValue("attachments.share_username","");
					String password =Configurator.getInstance().getPropValue("attachments.share_password","");
					String domain =Configurator.getInstance().getPropValue("attachments.share_domain","");
					logger.info("processing '" +  this.getAllegati().get(i) + "'");

					String attachPath =  VariousUtils.transformAttachPath(this.getAllegati().get(i));
					attachPath="smb://"+ host.replaceAll("\\\\\\\\", "").replaceAll("\\\\", "/")  + attachPath;

					logger.info("Connecting to '" + attachPath + "'");
					try {
						NtlmPasswordAuthentication authSmb;
						if(!user.isEmpty() && !user.isEmpty()) {
							logger.info("using NTLM authentication with user '" + domain +"\\" + user + "'");
							authSmb = new NtlmPasswordAuthentication(domain,user, password);						
						}else {
							logger.info("using anonymous authentication..");
							authSmb = NtlmPasswordAuthentication.ANONYMOUS;
						}
						SmbFile smbDestDirPath= new SmbFile(attachPath,authSmb);	
						if(smbDestDirPath.exists()) {
							addToZip(zipOut,smbDestDirPath,smbDestDirPath.getName());
							addToCommand(smbDestDirPath.getName());
							logger.info("Attachment added");
						}else {
							logger.warn("Attachment not found");
						}
					} catch (MalformedURLException e) {
						logger.error(e.getMessage(),e);
					} catch (SmbException e) {
						logger.error(e.getMessage(),e);
						//Recupera da folder locale di recovery
						if(Configurator.getInstance().getPropValue("attachments.recovery.enable","false").equals("true")) {
							logger.info("Attachment recovery folder is active! Checking for recovery attachments folder...");
							attachPath =  Configurator.getInstance().getPropValue("attachments.recovery.local_folder",".") + File.separator + this.getAllegati().get(i);
							File attachFile= new File(attachPath);	
							if(attachFile.exists()) {
								try {
									addToZip(zipOut,attachFile);
									addToCommand(attachFile.getName());
									logger.info("Attachment added");
								} catch (FileNotFoundException eSub) {
									logger.error("File not found!", eSub);							
								} catch (IOException eSub) {
									logger.error("File read error!",eSub);
								}
							}

						}
					} catch (IOException e) {
						logger.error(e.getMessage(),e);
					}
				}else {
					String attachPath =   VariousUtils.transformAttachPath(this.getAllegati().get(i));
					logger.info("sarching file " + attachPath);							
					File attachFile= new File(attachPath);	
					if(attachFile.exists()) {
						try {
							addToZip(zipOut,attachFile);
							addToCommand(attachFile.getName());
							logger.info("Attachment added");
						} catch (FileNotFoundException e) {
							logger.error("File not found!",e);							
						} catch (IOException e) {
							logger.error("File read error!",e);
						}
					}
				}
			}
		}

		for (String srcFile : srcFiles) {        	
			logger.debug("Add to zip " + srcFile);
			addToZip(zipOut, srcFile);
		}

		zipOut.close();
		fos.close();
		logger.debug("created zip " + this.getNomeFax() + ".zip");

		String faxOutFolder = Configurator.getInstance().getPropValue("job.ouput_fax_dir_path","outFax");

		if( !new File(faxOutFolder).exists()) {
			new File(faxOutFolder).mkdir();
		}
		for (String srcFile : srcFiles) {        	
			File currentFile = new File(srcFile);        	
			if(currentFile.delete())
				logger.debug(currentFile.getName() + " deleted." );
		}

		//backup
		String backupZipPath = creatorBackupOutZip.getGeneratedDirPath() + this.getNomeFax() + ".zip";		
		if(backupZipPath != null && !backupZipPath.isEmpty()) {
			Copier.copy(zipOutFile, new File(backupZipPath));
		}

		Copier.copy(zipOutFile, new File(faxOutFolder + "/" + zipOutFile.getName()));
		if(zipOutFile.delete())
			logger.debug(zipOutFile.getAbsolutePath() + " deleted." );
	}

	private void addToCommand(String name) throws IOException {
		FileWriter fw = new FileWriter(percorsoOutput + this.getNomeFax() + ".txt", true);
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter out = new PrintWriter(bw);
		out.println("ATTACH=" + name);
		out.close();
		bw.close();
	}

	private void addToZip(ZipOutputStream zipOut, SmbFile smbFilePath, String name) throws IOException {
		SmbFileInputStream smbFileInputStream = new SmbFileInputStream(smbFilePath);
		ZipEntry zipEntry = new ZipEntry(name);
		zipOut.putNextEntry(zipEntry); 
		byte[] bytes = new byte[1024];
		int length;
		while((length = smbFileInputStream.read(bytes)) >= 0) {
			zipOut.write(bytes, 0, length);
		}
		smbFileInputStream.close();
	}

	private void addToZip(ZipOutputStream zipOut, String srcFile) throws FileNotFoundException, IOException {
		File fileToZip = new File(srcFile);
		addToZip(zipOut , fileToZip);
	}

	private void addToZip(ZipOutputStream zipOut, File fileToAdd) throws FileNotFoundException, IOException {

		FileInputStream fis = new FileInputStream(fileToAdd);
		String nameInZipFile = fileToAdd.getName();

		if (fileToAdd.getName().equals(this.getNomeFax() + ".txt"))
			nameInZipFile = "command.txt";

		logger.debug("adding to zip '" + nameInZipFile + "'");

		ZipEntry zipEntry = new ZipEntry(nameInZipFile);
		zipOut.putNextEntry(zipEntry); 
		byte[] bytes = new byte[1024];
		int length;
		while((length = fis.read(bytes)) >= 0) {
			zipOut.write(bytes, 0, length);
		}
		fis.close();
	}

	//	BC=MTSEDEN
	//	ADDR=FAX;00390266118580
	//	ATTACH=test.pdf
	//	CREF=F1000001
	public void buildCommandTxt() throws IOException, BlackListException {
		File file = new File(percorsoOutput + this.getNomeFax() + ".txt");

		FileWriter writer = new FileWriter(file);
		PrintWriter printWriter = new PrintWriter(writer);
		String cdc;
		if(this.getCentroDiCosto().isEmpty())
			cdc = Configurator.getInstance().getPropValue("dd.centrodicosto","");
		else
			cdc =this.getCentroDiCosto();
		logger.debug("BC=" + cdc);
		printWriter.println("BC=" + cdc);

		String faxDest;
		if(!Configurator.getInstance().getPropValue("fax.test_fax_recipient","").isEmpty()) {
			faxDest = Configurator.getInstance().getPropValue("fax.test_fax_recipient","");
		}else {
			faxDest = this.getDestinatario();
		}

		if(!IsPermittedFax(faxDest)) {
			faxDest="BLACKLIST";			
		}

		logger.debug("ADDR=FAX;" + faxDest);
		printWriter.println("ADDR=FAX;" + faxDest); 
		logger.debug("CREF=" + this.getNomeFax());
		printWriter.println("CREF=" + this.getNomeFax());
		String nameOfPdf = new File(this.percorsoPdfGenerato).getName();
		logger.debug("ATTACH=" + nameOfPdf );
		printWriter.println("ATTACH=" + nameOfPdf);
		printWriter.close();
		writer.close();			
	}

	private boolean IsPermittedFax(String destinatario) {
		String permittedFax = Configurator.getInstance().getPropValue("fax.permitted_fax_recipient","*");
		if(permittedFax.equals("*"))
			return true;

		StringTokenizer st = new StringTokenizer(permittedFax, ";");
		String currentFax;
		while(st.hasMoreTokens()) {
			currentFax = st.nextToken();
			logger.debug("current domain to check is:" +  currentFax);
			if(destinatario.trim().toLowerCase().endsWith(currentFax.trim().toLowerCase())) {
				logger.debug(destinatario + " fax is permitted");
				return true;
			}
		}	
		logger.debug(destinatario + " fax is not permitted!");
		return false;
	}
	
}
