/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.messaggi;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;


public class MessaggioDb {

	//	id integer identity,
	//	tipo varchar(3) not null,
	//	percorso varchar(150) not null,
	//	titolo varchar(150) not null,
	//	reparto varchar(150),
	//	generato TIMESTAMP not null,
	//	riferimento  varchar(150) not null,
	//	logo  varchar(150) not null,
	//	utente  varchar(150) not null,
	//	layout  varchar(150) not null,
	//	oggetto varchar(150),
	//	archive_path varchar(500)
	private String tipo;
	private String percorso;
	private String titolo;
	private String reparto;
	private Timestamp generato;	
	private String riferimento;

	private String logo;
	private String utente;
	private String layout;
	private String oggetto;
	private String archivePath;
	private String backupFile;


	public MessaggioDb(String tipo,String percorso,String titolo,String reparto,Timestamp generato,String riferimento,
			String logo,String utente,String layout,String oggetto,String archivePath,String backupFile) {
		super();
		this.tipo=tipo.replaceAll("#", "");
		this.percorso=percorso;
		this.titolo=titolo;
		this.reparto=reparto;
		this.generato=generato;
		this.riferimento=riferimento;
		this.logo=logo;
		this.utente=utente;
		this.layout=layout;
		this.oggetto=oggetto;
		this.archivePath=archivePath;
		this.backupFile=backupFile;
	}
	public MessaggioDb(AbstractMessaggio messaggio){
		this(messaggio.getTipo(),
				messaggio.getDestinatario(),
				messaggio.getNomeFax(),
				messaggio.getCentroDiCosto(),
				messaggio.getTimestamp(),
				messaggio.getNumeroRiferimento(),
				messaggio.getLogo(),
				messaggio.getUtente(),
				messaggio.getFormattazione(),
				messaggio.getOggetto(),
				messaggio.getArchiveUrl(),
				messaggio.getBackupFilePath());			
	}


	public String getTipo() {
		return tipo;
	}
	public String getPercorso() {
		return percorso;
	}
	public String getTitolo() {
		return titolo;
	}
	public String getReparto() {
		return reparto;
	}
	public Timestamp getGenerato() {
		return generato;
	}
	public String getRiferimento() {
		return riferimento;
	}
	public String getLogo() {
		return logo;
	}
	public String getUtente() {
		return utente;
	}
	public String getLayout() {
		return layout;
	}
	public String getOggetto() {
		return oggetto;
	}
	public String getArchivePath() {
		return archivePath;
	}

	public String getBackupFile() {
		if(backupFile==null)
			return "";
		return backupFile;
	}

	public Object getCampo(int columnIndex){
		switch (columnIndex){
		case 0:
			return tipo;
		case 1:
			return percorso;
		case 2:
			return titolo;
		case 3:
			return reparto;
		case 4:
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sdf.format(getGenerato());
		case 5:
			return riferimento;
		case 6:
			return logo;
		case 7:
			return utente;
		case 8:
			return layout;
		case 9:
			return oggetto;
		case 10:
			return getArchivePath();
		case 11:		
			return getBackupFile();
		}
		return null;
	}

}
