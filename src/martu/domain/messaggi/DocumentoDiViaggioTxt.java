/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.messaggi;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import martu.prettyspooler.domain.DocsViaggioGenerator;
import martu.utils.Configurator;
import martu.utils.Copier;
import martu.utils.CreaCartelle;


public class DocumentoDiViaggioTxt extends AbstractMessaggio implements DocumentoDiViaggio{

	private final Logger logger = LogManager.getLogger(DocumentoDiViaggioTxt.class);
	public String company;
	private boolean isSecondDir = false;

	public DocumentoDiViaggioTxt(File file,String company)throws NullPointerException, IOException, ParserConfigurationException, SAXException, TransformerException{
		super(file);
		this.company=company;
		creatorBackupTxtGenericDocuments=new CreaCartelle("txt/backup/GenericDocuments" +  File.separator + company );
		backupFilePath = creatorBackupTxtGenericDocuments.getGeneratedDirPath() +  file.getName();
		if(!backupFilePath.isEmpty())
			Copier.copy(file, new File(backupFilePath));
		genera(file);		
	}

	public void genera(File file) throws IOException {
		tokenizerDir(getDestinatario());
		//generaPdf();
		DocsViaggioGenerator converter = new DocsViaggioGenerator(file);
		converter.setLogo(Configurator.getInstance().getPropValue("pdf." + this.getLogo().toLowerCase(),"logo01.jpg"));
		//converter.setLogo(Configurator.getInstance().getPropValue("document.logo." + company,"logo01.jpg"));
		ByteArrayOutputStream pdfStream = converter.generaPdf();
		converter.getInput().close();

		Path path = Paths.get(Configurator.getInstance().getPropValue("document.output_folder."+ company,"documents") 
				+ File.separator + this.getFirstDirToCreate() 
				+ File.separator  +  this.getSecondDirToCreate());

		if (!Files.exists(path)) {
			logger.info("creating path " + path);
			Files.createDirectories(path);			
		}

		String outputPath = path + File.separator + FilenameUtils.removeExtension(file.getName()) + ".pdf";
		FileOutputStream fos = new FileOutputStream(outputPath);
		fos.write(pdfStream.toByteArray());
		fos.close();

		logger.info("PDF created on '" + outputPath + "'");
	}
	
	@Override
	public String getFirstDirToCreate() {
		return firstDir;
	}
	@Override
	public String getSecondDirToCreate() {
		return secondDir;
	}
	@Override
	public void tokenizerDir(String percorso){
		try{
			StringTokenizer st=new StringTokenizer(percorso, "/");
			firstDir=st.nextToken();			
			secondDir=st.nextToken();
			isSecondDir=true;
		}catch(NoSuchElementException e){
			logger.error(e);
		}
	}

	@Override
	public boolean isSecondDir() {
		return isSecondDir;
	}
	
}