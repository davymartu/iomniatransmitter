/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.messaggi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FilenameUtils;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xmlgraphics.util.MimeConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import martu.utils.Configurator;
import martu.utils.Copier;
import martu.utils.CreaCartelle;


public class DocumentoDiViaggioXml implements DocumentoDiViaggio{

	private final Logger logger = LogManager.getLogger(DocumentoDiViaggio.class);
	public String company;
	private String firstDir;
	private String secondDir;
	private boolean isSecondDir = false;
	private CreaCartelle creatorBackupTxtGenericDocuments;
	private String backupFilePath;
	private String outputPdfPath;

	public DocumentoDiViaggioXml(File file,String company)throws NullPointerException, IOException, ParserConfigurationException, SAXException, TransformerException{
		this.company=company;
		creatorBackupTxtGenericDocuments=new CreaCartelle("txt/backup/GenericDocuments" +  File.separator + company );
		backupFilePath = creatorBackupTxtGenericDocuments.getGeneratedDirPath() +  file.getName();
		if(!backupFilePath.isEmpty())
			Copier.copy(file, new File(backupFilePath));
		
		genera(file);
	}


	public void genera(File inputXML) throws 
	IOException, ParserConfigurationException, SAXException, TransformerException {

		DocumentBuilderFactory fact = 
				DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = fact.newDocumentBuilder();
		Document doc = builder.parse(inputXML);
		Element node = doc.getDocumentElement();
		String tipoDoc = node.getNodeName();
		String outputDir = node.getAttribute("docUrl");
		
		logger.info("Root Node: " + tipoDoc);
		logger.info("docUrl: " + outputDir);
		
		File fopTemplate = new File("config/xsl-fo-templates/" + tipoDoc + ".xsl");
		
		if(!fopTemplate.exists())
			throw new FileNotFoundException();
		
		tokenizerDir(outputDir);

		Path path = Paths.get(Configurator.getInstance().getPropValue("document.output_folder."+ company,"documents") 
				+ File.separator + this.getFirstDirToCreate() 
				+ File.separator  +  this.getSecondDirToCreate());

		if (!Files.exists(path)) {
			logger.info("creating path " + path);
			Files.createDirectories(path);			
		}

		outputPdfPath = path + File.separator + FilenameUtils.removeExtension(inputXML.getName()) + ".pdf";
		transformXMLToPDF(inputXML,fopTemplate,new File(outputPdfPath));
		//scrivi 
		logger.info("PDF created on '" + outputPdfPath + "'");
	}

	@Override
	public String getFirstDirToCreate() {
		return firstDir;
	}

	@Override
	public String getSecondDirToCreate() {
		return secondDir;
	}

	@Override
	public void tokenizerDir(String percorso){
		try{
			StringTokenizer st=new StringTokenizer(percorso, "/");
			st.nextToken();	
			firstDir=st.nextToken();			
			secondDir=st.nextToken();
			isSecondDir=true;
		}catch(NoSuchElementException e){
			logger.error(e);
		}
	}

	@SuppressWarnings("unused")
	private void transformIntermed(File dataXML, File inputXSL, File outputXML)
			throws TransformerConfigurationException, TransformerException, IOException{

		TransformerFactory factory = TransformerFactory.newInstance();
		StreamSource xslStream = new StreamSource(inputXSL);
		Transformer transformer = factory.newTransformer(xslStream);
		StreamSource in = new StreamSource(dataXML);
		StreamResult out = new StreamResult(outputXML);
		transformer.transform(in, out); /* applica lo stile e crea il file sul filesystem */

		/* stampo a console il contentuto del nuovo file XML */
		logger.info("XML generato:" + outputXML);
		//		System.out.println("\nLeggo XML creato:\n");
		//		BufferedReader xmlCreato = new BufferedReader(new FileReader(outputXML));
		//		String l;
		//		while((l = xmlCreato.readLine()) != null ){
		//			System.out.println(l);
		//		}
		//		xmlCreato.close();

	}

	private void transformXMLToPDF(File xmlFile, File xsltFile,File pdfFile) throws 
	FOPException, TransformerException, IOException {
		// configure fopFactory as desired
		final FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());

		FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
		// configure foUserAgent as desired

		// Setup output
		OutputStream out = new java.io.FileOutputStream(pdfFile);
		out = new java.io.BufferedOutputStream(out);

		//		try {
		// Construct fop with desired output format
		Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

		// Setup XSLT
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));

		// Set the value of a <param> in the stylesheet
		transformer.setParameter("versionParam", "2.0");

		// Setup input for XSLT transformation
		Source src = new StreamSource(xmlFile);

		// Resulting SAX events (the generated FO) must be piped through to FOP
		Result res = new SAXResult(fop.getDefaultHandler());

		// Start XSLT transformation and FOP processing
		transformer.transform(src, res);
		
		out.close();
		//		} finally {
		//		out.close();
		//		}
	}

	@Override
	public boolean isSecondDir() {
		return isSecondDir;
	}

	@Override
	public String getPercorsoPdfGenerato() {
		return outputPdfPath;
	}

}