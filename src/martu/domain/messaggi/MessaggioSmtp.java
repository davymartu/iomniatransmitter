/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.messaggi;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.mail.MessagingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import martu.domain.services.SmtpMailManager;
import martu.utils.Configurator;
import martu.utils.Copier;

public class MessaggioSmtp extends AbstractMessaggio {
	private String company = "default";
	private final Logger logger = LogManager.getLogger(MessaggioSmtp.class);
	private  HashMap<String,String> mapOfMailParams = new HashMap<String,String>();
	public boolean isAttachNotFound=false;
	private String dataOrigine;
	public boolean isAllegatoTestoActive=false;

	public HashMap<String,String> getMailParams(){
		return mapOfMailParams;
	}

	public String getCompany() {
		return company;
	}

	public String getDataOrigine() {
		return dataOrigine;
	}

	//�Q=e-mail SMTP� e �E= e-mail SMTP con allegato�
	public MessaggioSmtp(File file,String tipoPrefix) throws AllegatoNotFoundException{
		super(file);

		SimpleDateFormat data_format=new SimpleDateFormat("dd/MM");
		SimpleDateFormat ora_format =new SimpleDateFormat("HH:mm:ss");
		String dataDB=data_format.format(new GregorianCalendar().getTime());
		String oraDB=ora_format.format(new GregorianCalendar().getTime());		
		dataOrigine=data.substring(0,2)+"/"+data.substring(3,5)+" " +data.substring(9,14);

		try {

			//Se le companies sono pi� di una e la prima � diversa da "default" uso il codice reparto/centro di costo come configurazione SMTP
			if(Configurator.getInstance().getSMTPCompanies().size() > 1 ) { //&& Configurator.getInstance().getSMTPCompanies().get(0).toLowerCase().equals("default")
				logger.debug("searching company...");
				Iterator<String> itCompanies = Configurator.getInstance().getSMTPCompanies().iterator();
				while(itCompanies.hasNext()) {
					String currentCompany = itCompanies.next().toLowerCase();
					logger.debug("current company config:" + currentCompany);
					if(Configurator.getInstance().getPropValue("smtp.companies.discriminator","").equalsIgnoreCase("LOGO")) {
						logger.debug("comparing with logo message:" + this.getLogo().toLowerCase());
						if(Configurator.getInstance().getPropValue("smtp.companies.discriminator." + this.getLogo(),"").equalsIgnoreCase(currentCompany)) {
							//if(Configurator.getInstance().getPropValue("smtp.companies.discriminator." + this.getLogo(),"").equalsIgnoreCase(currentCompany)) {
							logger.info("found SMTP company match for this message LOGO:" + currentCompany);
							this.company = currentCompany;
							//}
						}else {
							logger.debug("not matched!");
						}
					}else {					
						if(this.getCentroDiCosto().toLowerCase().equals(currentCompany)) {
							logger.info("found SMTP company match for this message:" + currentCompany);
							this.company = currentCompany;
						}
					}
				}

			}
			logger.debug("Company SMTP for this message:" + this.company);
			testoDellaMail="";
			String linea = input.readLine();

			if (linea.trim().equals("&&ALLEGATESTO:SI")) {
				logger.debug("Found &&ALLEGATESTO:SI, enabling TXT attachment");
				isAllegatoTestoActive = true;
			}
			
			if(linea.equals("<body>") || tipoPrefix.equals("Q") || tipoPrefix.equals("I")){
				if( linea.contains("%%") && getTipo().equals("#INT#")){
					testoDellaMail = addAllegatoAndCleanRow(linea) +"\n";					
				}else if(linea.contains("$$")){
					addMailParams(linea);					
				}else if(!isAllegatoTestoActive){
					testoDellaMail = linea +"\n";
				}

				for(linea = input.readLine(); linea!=null && !linea.equals("</body>") ; linea = input.readLine())
				{
					if( linea.contains("%%") && getTipo().equals("#INT#")){
						testoDellaMail+=addAllegatoAndCleanRow(linea) +"\n";
					}else if( linea.contains("$$")){
						addMailParams(linea);
					}else{
						testoDellaMail+=linea +"\n";
					}
				}
				testoDellaMail = testoDellaMail.replaceAll("�", "�");
			}else if( linea.contains("$$")){				
				addMailParams(linea);
			}else {
				setPrimaRiga(linea);
			}

			if(!tipoPrefix.equals("Q") && !tipoPrefix.equals("I"))
				generaPdf();


			dataDB=data_format.format(new GregorianCalendar().getTime());
			oraDB=ora_format.format(new GregorianCalendar().getTime());		
			dataOrigine=data.substring(0,2)+"/"+data.substring(3,5)+" " +data.substring(9,14);

			// GENERAZIONE E INVIO MAIL
			if(tipo.equals("#INT#")){
				for(int i=0; i<allegati.size();i++){
					logger.debug("Allegato " + i + ":" + allegati.get(i));
				}
				try {
					//percorsoPdfGenerato,nomeFax,mittente,destinatario,centroDiCosto,oggetto,data,numeroRiferimento,mailMittente
					SmtpMailManager.getInstance().send(this);
					logger.info(nomeFax +" mandato all'email "+ destinatario);
					GeneratoreNotifiche.scriviNotificaLocale(dataDB, oraDB, numeroRiferimento,  tipo, "OK","1","0",centroDiCosto,dataOrigine);
				} catch (MessagingException e) {
					logger.warn(nomeFax +" invio all'email "+ destinatario +" Fallito!");
					logger.error(e.getMessage(),e);
					GeneratoreNotifiche.scriviNotificaLocale(dataDB, oraDB, numeroRiferimento,  tipo, "SMT","1","0",centroDiCosto,dataOrigine);
					//db.insertDB(dataDB, oraDB, Integer.parseInt(numeroRiferimento), tipo, "EMA","1","0",percorsoFilePdf);
				} catch (NumberFormatException e) {
					logger.error(e.getMessage(),e);
					GeneratoreNotifiche.scriviNotificaLocale(dataDB, oraDB, numeroRiferimento,  tipo, "ERR","1","0",centroDiCosto,dataOrigine);
				} catch (AllegatoNotFoundException e) {
					if(Configurator.getInstance().getPropValue("attachments.wait_attach_strategy","false").equals("true")) {
						logger.warn(e.getMessage());		
						try {
							input.close();
						} catch (IOException e1) {
							logger.error("Non sono riuscito a chiudere il file");
						}
						this.isAttachNotFound = true;
						//throw new AllegatoNotFoundException(e.getMessage());
					} else {
						logger.error(e.getMessage(),e);					
						GeneratoreNotifiche.scriviNotificaLocale(dataDB, oraDB, numeroRiferimento,  tipo, "ALL","1","0",centroDiCosto,dataOrigine);
					}
				} catch (Exception e) {
					logger.error(e.getMessage(),e);
					GeneratoreNotifiche.scriviNotificaLocale(dataDB, oraDB, numeroRiferimento,  tipo, "GEN","1","0",centroDiCosto,dataOrigine);
				}
			}else{
				GeneratoreNotifiche.scriviNotificaLocale(dataDB, oraDB, numeroRiferimento, tipo, "FAX","1","0",centroDiCosto,dataOrigine);
			}
			if(percorsoPdfGenerato != null) {
				//Copier.copy(new File(percorsoPdfGenerato),new File (creatorBackupOutPdf.getGeneratedDirPath() + new File(percorsoPdfGenerato).getName()));
				new File(percorsoPdfGenerato).delete();
			}
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			GeneratoreNotifiche.scriviNotificaLocale(dataDB, oraDB, numeroRiferimento,  tipo, "ERR","1","0",centroDiCosto,dataOrigine);
		} catch (NullPointerException e) {
			logger.error(e.getMessage(),e);
			logger.error("Errore formato file ricevuto! Numero riferimento:"+ this.getNumeroRiferimento());
			Copier.copy(file,new File (creatorBackupErrTxt.getGeneratedDirPath() + file.getName()));			
			//			if(out != null) 
			//				out.close();
			//			if(messaggioFile != null)
			//				messaggioFile.delete();
			if(input!=null)
				try {
					input.close();
				} catch (IOException e1) {
					logger.error("Non sono riuscito a chiudere il file");
				}
			if(!this.getNumeroRiferimento().isEmpty())
				GeneratoreNotifiche.scriviNotificaLocale(dataDB, oraDB, numeroRiferimento,  tipo, "ERR","1","0",centroDiCosto,dataOrigine);

		}
	}

	private void addMailParams(String linea) {
		logger.debug("found mail param:" +linea );
		try {
			String lineaPulita = linea.trim().substring(2,linea.trim().length() - 2);
			logger.debug("clean mail param:" +lineaPulita );
			String key = lineaPulita.substring(0,lineaPulita.indexOf(":")).toLowerCase();
			String value = lineaPulita.substring(lineaPulita.indexOf(":") + 1,lineaPulita.length());
			logger.debug("adding key <" +key +"> , value <" + value + ">" );
			this.mapOfMailParams.put(key, value);
		}catch(Exception e ) {
			logger.error(e);
		}
	}

}
