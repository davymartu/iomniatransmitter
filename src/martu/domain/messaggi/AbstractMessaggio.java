/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.messaggi;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import martu.utils.Configurator;
import martu.utils.Copier;
import martu.utils.CreaCartelle;

public class AbstractMessaggio  {


	private final Logger logger = LogManager.getLogger(AbstractMessaggio.class);

	public static final String percorsoOutput="out/";
	public static final String percorsoInTxt="txt/";
	
	String docNotesRegex = "^&(\\w+);(\\d*);?([0-9\\-\\/]*);?([0-9\\-\\/]*);?";
	
	public String testoDellaMail = "";
	protected String formattazione;
	protected String logo;
	protected String tipo;
	protected String destinatario;
	protected String nomeFax;
	protected String centroDiCosto;
	protected String data;
	protected String oggetto;
	protected String mittente;
	protected String nomeMittente;
	protected String numeroRiferimento;
	protected String utente;
	protected String orientamentoPagina;
	protected float fontDim;
	protected String primaRiga;
	protected String logoSRC;
	protected BufferedReader input;
	protected CreaCartelle creatorBackupTxtEmail;
	protected CreaCartelle creatorBackupTxtFax;
	protected CreaCartelle creatorBackupOutTxt;
	public CreaCartelle creatorBackupOutPdf;
	protected CreaCartelle creatorBackupErrTxt;
	protected ArrayList<String> allegati;
	//	protected File messaggioFile;
	private File fileSourceTXT;
	public String percorsoPdfGenerato;
	private Font myFont;
	protected String firstDir;
	protected String secondDir;
	protected boolean pdfUnico =false;
	protected CreaCartelle creatorBackupTxtGenericDocuments;
	private String archiveUrl;
	protected String backupFilePath;
	protected String firstArchiveDir;
	protected String secondArchiveDir;
	private int maxColumns;

	private final String allegatoRegex = "(?<allegato>%%.+%%)";
	private Pattern patternAllegatoRegex = Pattern.compile(allegatoRegex);

	private String ccMailRecipients="";


	public String getCcMailRecipients() {
		return ccMailRecipients;
	}

	public void setCcMailRecipients(String ccMailRecipients) {
		this.ccMailRecipients = ccMailRecipients;
	}

	public String getArchiveUrl() {
		return archiveUrl;
	}

	/**
	 * @return the percorsoPdfGenerato
	 */
	public String getPercorsoPdfGenerato() {
		return percorsoPdfGenerato;
	}

	/**
	 * @return the allegati
	 */
	public ArrayList<String> getAllegati() {
		return allegati;
	}

	public String addAllegatoAndCleanRow(String riga){
		StringBuffer newRiga = new StringBuffer();
		Matcher matcher = patternAllegatoRegex.matcher(riga);
		try {
			while (matcher.find()) {
				String allegato= matcher.group("allegato");
				logger.debug("found allegato:" + allegato);
				allegati.add(allegato.replaceAll("%", "").trim());
				String rep = StringUtils.repeat(" ",allegato.length());
				matcher.appendReplacement(newRiga, rep);
			}
			matcher.appendTail(newRiga);
			logger.debug("cleaned row is:" + newRiga.toString());
		}catch(IllegalStateException e) {
			logger.error(e.getMessage(), e);
		}
		return newRiga.toString();
	}

	public String getBackupFilePath() {
		return backupFilePath;
	}

	/**
	 * @return the formattazione
	 */
	public String getFormattazione() {
		return formattazione;
	}

	/**
	 * @param formattazione the formattazione to set
	 */
	public void setFormattazione(String formattazione) {
		this.formattazione = formattazione;
	}

	/**
	 * @return the logo
	 */
	public String getLogo() {
		return logo;
	}

	/**
	 * @param logo the logo to set
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the destinatario
	 */
	public String getDestinatario() {
		return destinatario;
	}

	/**
	 * @param destinatario the destinatario to set
	 */
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	/**
	 * @return the nomeFax
	 */
	public String getNomeFax() {
		return nomeFax;
	}

	/**
	 * @param nomeFax the nomeFax to set
	 */
	public void setNomefax(String nomefax) {
		this.nomeFax = nomefax;
	}

	/**
	 * @return the centroDiCosto
	 */
	public String getCentroDiCosto() {
		return centroDiCosto;
	}

	/**
	 * @param centroDiCosto the centroDiCosto to set
	 */
	public void setCentroDiCosto(String centroDiCosto) {
		this.centroDiCosto = centroDiCosto;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * @return the mittente
	 */
	public String getMittente() {
		return mittente;
	}

	/**
	 * @param mittente the mittente to set
	 */
	public void setMittente(String mittente) {
		this.mittente = mittente;
	}

	/**
	 * @return the numeroRiferimento
	 */
	public String getNumeroRiferimento() {
		return numeroRiferimento;
	}

	/**
	 * @param numeroRiferimento the numeroRiferimento to set
	 */
	public void setNumeroRiferimento(String numeroRiferimento) {
		this.numeroRiferimento = numeroRiferimento;
	}

	/**
	 * @return the utente
	 */
	public String getUtente() {
		return utente;
	}

	/**
	 * @param utente the utente to set
	 */
	public void setUtente(String utente) {
		this.utente = utente;
	}

	/**
	 * @return the orientamentoPagina
	 */
	public String getOrientamentoPagina() {
		return orientamentoPagina.toUpperCase();
	}

	/**
	 * @param orientamentoPagina the orientamentoPagina to set
	 */
	public void setOrientamentoPagina(String orientamentoPagina) {
		this.orientamentoPagina = orientamentoPagina;
	}

	/**
	 * @return the fontDim
	 */
	public float getFontDim() {
		return fontDim;
	}

	/**
	 * @param fontDim the fontDim to set
	 */
	public void setFontDim(float fontDim) {
		this.fontDim = fontDim;
	}

	/**
	 * @return the primaRiga
	 */
	public String getPrimaRiga() {
		return primaRiga;
	}

	/**
	 * @param primaRiga the primaRiga to set
	 */
	public void setPrimaRiga(String primaRiga) {
		this.primaRiga = primaRiga;
	}

	/**
	 * @return the logoSRC
	 */
	public String getLogoSRC() {
		return logoSRC;
	}

	/**
	 * @param logoSRC the logoSRC to set
	 */
	public void setLogoSRC(String logoSRC) {
		this.logoSRC = logoSRC;
	}

	/**
	 * @return the input
	 */
	public BufferedReader getInput() {
		return input;
	}

	public Timestamp getTimestamp(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Timestamp timestamp;
		try {
			java.util.Date parsedDate = dateFormat.parse(getData());
			timestamp = new java.sql.Timestamp(parsedDate.getTime());
		} catch (ParseException e) {
			logger.warn("Non sono riuscito a capire la data del messaggio!");
			timestamp=new Timestamp(new GregorianCalendar().getTimeInMillis());
		}
		return timestamp;
	}

	/**
	 * @param input the input to set
	 */
	public void setInput(BufferedReader input) {
		this.input = input;
	}

	public String getNomeMittente() {
		return nomeMittente;
	}

	public void setNomeMittente(String nomeMittente) {
		this.nomeMittente = nomeMittente;
	}

	public AbstractMessaggio(){
		allegati=new ArrayList<String>();
		creatorBackupTxtEmail=new CreaCartelle("txt/backup/email");
		creatorBackupTxtFax=new CreaCartelle("txt/backup/fax");
		//creatorBackupTxtGenericDocuments=new CreaCartelle("txt/backup/GenericDocuments");
		creatorBackupOutTxt=new CreaCartelle("txt/backup/outTxt");
		creatorBackupOutPdf=new CreaCartelle("txt/backup/outPdf");
		creatorBackupErrTxt=new CreaCartelle("txt/backup/errorsTxt");		
	}

	public AbstractMessaggio(File file){
		this();
		this.setFileSourceTXT(file);
		logger.info("Messaggio processato: "+ file.getName());
		try {

			input = new BufferedReader(new FileReader(file));
			ricavaInfo(input.readLine());
			//ArchivioMessaggi.insertMessaggio(this);
			logger.info(this.toString());
			backup();

		} catch (IOException e) {
			logger.error(e.getMessage());
		} catch (NullPointerException e) {
			logger.error("Errore formato file ricevuto! Numero riferimento:" + this.getNumeroRiferimento());
			Copier.copy(file,new File (creatorBackupErrTxt.getGeneratedDirPath() + file.getName()));
			try {
				input.close();
			} catch (IOException e1) {
				logger.error(e.getMessage());
			}
		}
	}

	private void backup() {
		if(getTipo().equals("#INT#"))
			backupFilePath=creatorBackupTxtEmail.getGeneratedDirPath() + getFileSourceTXT().getName();
		//		if(getTipo().equals("#PDF#"))
		//			backupFilePath=creatorBackupTxtGenericDocuments.getGeneratedDirPath() + file.getName();
		if(getTipo().equals("#FAX#"))
			backupFilePath=creatorBackupTxtFax.getGeneratedDirPath() + getFileSourceTXT().getName();

		if(backupFilePath != null && !backupFilePath.isEmpty())
			Copier.copy(getFileSourceTXT(), new File(backupFilePath));
	}


	public void ricavaInfo(String linea){
		try{
			mittente="";
			nomeMittente="";
			StringTokenizer st = new StringTokenizer(linea,",");
			tipo=st.nextToken();
			tipo=tipo.replaceAll("\"","");
			destinatario=st.nextToken();
			destinatario=destinatario.replaceAll("\"","");
			nomeFax=st.nextToken();
			nomeFax=nomeFax.replaceAll("\"","");
			nomeFax=nomeFax.replaceAll("/","_");
			try
			{
				nomeFax=nomeFax.substring(0, nomeFax.indexOf('.'));
			}catch(StringIndexOutOfBoundsException e){

			}

			centroDiCosto=st.nextToken();
			centroDiCosto=centroDiCosto.replaceAll("\"","");
			data=st.nextToken();
			data=data.replaceAll("\"","");
			numeroRiferimento=st.nextToken();
			numeroRiferimento=numeroRiferimento.replaceAll("\"","");
			logo=st.nextToken();
			logo=logo.replaceAll("\"","");
			logoSRC=Configurator.getInstance().getLogo(logo);
			utente=st.nextToken();
			utente=utente.replaceAll("\"","");

			if(!utente.isEmpty())
				this.setNomeMittente(utente);

			formattazione=st.nextToken();
			formattazione=formattazione.replaceAll("\"","");
			try{
				orientamentoPagina=formattazione.substring(0, formattazione.indexOf(':'));
				fontDim=Float.parseFloat(formattazione.substring(formattazione.indexOf(':') + 1, formattazione.length()));
			}catch(StringIndexOutOfBoundsException e){
				orientamentoPagina="V";
				try{
					fontDim=Float.parseFloat(formattazione);
				}catch(java.lang.NumberFormatException b){
					fontDim=12;
				}
			}
			if(st.hasMoreElements()){
				oggetto=st.nextToken();
				oggetto=oggetto.replaceAll("\"","");
			}
			else{
				//Oggetto di Default tolto
				oggetto="";
			}
			if(st.hasMoreElements()){
				mittente=st.nextToken();
				mittente=mittente.replaceAll("\"","");
			}
			//			else
			//				mittente=Configurator.getInstance().getPropValue("mail.default_sender","");

			if(mittente.equals(""))
				mittente=Configurator.getInstance().getPropValue("smtp.default_sender","");

			if(st.hasMoreElements()){
				ccMailRecipients=st.nextToken();
				ccMailRecipients=ccMailRecipients.replaceAll("\"","");
			}

		}catch(StringIndexOutOfBoundsException e){
			logger.error(e.getMessage());
		}catch(NoSuchElementException e){
			logger.error(e.getMessage());
		}
	}

	//	private void ricavaLogo(String tempLogo) {
	//		logo=
	//		try{
	//			FileInputStream in = new FileInputStream ("loghi.txt");
	//			Properties  prop = new Properties();
	//			prop.load(in);
	//			logo=(prop.getProperty(logo));
	//			if(logo==null)
	//				logo=(prop.getProperty("default"));
	//			in.close();
	//		}catch(FileNotFoundException e){
	//			logger.error(e.getMessage());
	//		} catch (IOException e) {
	//			logger.error(e.getMessage());
	//		}
	//	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ("Numero riferimento:"+ getNomeFax() + " \ttipo messaggio:" +getTipo());
	}

	public void generaPdf()throws IOException{
		Document document;
		String linea;

		if(getOrientamentoPagina().equals("O"))
			document = new Document(PageSize.A4.rotate(),20,20,15,25);		
		else
			document = new Document(PageSize.A4,20,20,15,25);

		//creazione documento Document(pagesize, margin left,margin right, margin top ,margin bottom
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			myFont= new Font(Font.FontFamily.COURIER, getFontDim());
			//creazione writer
			PdfWriter pdfWriter = PdfWriter.getInstance(document,	baos);
			//Apertura documento
			document.open();
			//Aggiunta immagine
			printLogo(document);
			//Metti la guardia
			this.getMaxColumns();

			Paragraph separator = new Paragraph(0);
			separator.add(new Chunk(new LineSeparator()));
			separator.setSpacingBefore(3);
			separator.setSpacingAfter(3);
			document.add(separator);
			if(getPrimaRiga()!=null && !getPrimaRiga().contains("%%") && !getPrimaRiga().trim().equals("&&ALLEGATESTO:SI")){
				//se no si mangia la prima riga
				/*Paragraph c=new Paragraph(getPrimaRiga(),myFont);
				c.setAlignment(Element.ALIGN_LEFT);
				c.setLeading(getFontDim());
				document.add(c);*/
				document.add(stringToPhrase(getPrimaRiga()));
				setPrimaRiga("");
			}else if(getPrimaRiga()!=null && getPrimaRiga().contains("%%")){
				setPrimaRiga(addAllegatoAndCleanRow(getPrimaRiga()));
				//				addAllegato(getPrimaRiga());
				//				if(this instanceof MessaggioDigitalDox){
				//					if(pdfUnico)
				//						((MessaggioDigitalDox)this).addAllegato(getPrimaRiga());
				//					else
				//						scriviRiga(getPrimaRiga());
				//				}
			}else if(getPrimaRiga()!=null && getPrimaRiga().trim().equals("&&ALLEGATESTO:SI")) {
				setPrimaRiga("");
			}else if (getPrimaRiga()!=null && getPrimaRiga().trim().matches(docNotesRegex)){
				String html = getHtmlTemplateString(getPrimaRiga().trim());					
				InputStream is = new ByteArrayInputStream(html.getBytes());
				XMLWorkerHelper.getInstance().parseXHtml(pdfWriter, document, is);
				is.close();
				setPrimaRiga("");
			}
			//Conto le righe
			//int n=0;
			//for(linea = getInput().readLine(); linea!=null; linea = getInput().readLine())
			//{
			//				n++;
			//			}
			//			System.out.println("Numero righe testo:"+n);
			//			//Istanzio nuovamente il bufferedReader
			//			input = new BufferedReader(new FileReader(file));
			//			//scarto la prima riga
			//			getInput().readLine();
			//Aggiunta paragrafo
			long nRiga = 0;

			for(linea = getInput().readLine(); linea!=null; linea = getInput().readLine())
			{
				nRiga++;
				//				document.add(Chunk.NEWLINE);
				if(linea.contains("%%")){
					//					if(pdfUnico){					
					String contenuto = addAllegatoAndCleanRow(linea);						
					document.add(stringToPhrase(contenuto));
					//					}
					//					else
					//						scriviRiga(linea);
				}
				else{

					if((linea.trim().equals(":&PAGE")||linea.trim().equals(".PG") ) && nRiga > 1 ){   // || linea.replaceAll("\\s+$","").equals(".")
						document.newPage();
						printLogo(document);
						//document.add(jpg);
						document.add(separator);
					}else if(!linea.equals("NNNN")){
						//comodo=comodo + linea + "\n ";
						//Copia in altra variabile per bug encoding � � �
						if(linea.equals(":&PAR")){
							String contenuto = "";
							for(linea = getInput().readLine(); !linea.equals(":&ENDPAR"); linea = getInput().readLine())
							{
								contenuto+=linea;
							}
							Paragraph c=new Paragraph(contenuto,myFont);
							c.setAlignment(Element.ALIGN_LEFT);
							c.setLeading(getFontDim());
							c.setKeepTogether(true);
							document.add(c);
						}else if(linea.equals("")){
							String contenuto=" ";							
							document.add(stringToPhrase(contenuto));
						}else if (linea.contains(":&FIRMA")){
							Image firmaJpg = Image.getInstance("images/firma.jpg");
							firmaJpg.setAlignment(Element.ALIGN_LEFT);
							firmaJpg.scaleAbsolute(firmaJpg.getWidth()/2,firmaJpg.getHeight()/2); // Code 2
							document.add(firmaJpg);
						}else if(linea.replaceAll("\\s+$","").equals(".")){
							//DoNothing
							document.add(stringToPhrase(""));
						}else if (linea.trim().matches(docNotesRegex)){
							String html = getHtmlTemplateString(linea.trim());					
							InputStream is = new ByteArrayInputStream(html.getBytes());
							XMLWorkerHelper.getInstance().parseXHtml(pdfWriter, document, is);
							is.close();
						}else{
							String contenuto=linea;							
							document.add(stringToPhrase(contenuto));
						}
					}
				}
			}
		} catch (DocumentException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		//Chiusura documento
		document.close();
		try {
			PdfReader reader = new PdfReader(baos.toByteArray());
			int n = reader.getNumberOfPages();
			PdfStamper stamper;

			stamper = new PdfStamper(reader, new FileOutputStream(percorsoOutput + getNomeFax() + "tmp.pdf"));

			PdfContentByte page;
			Rectangle rect;
			BaseFont bf = BaseFont.createFont();
			for (int i = 1; i < n + 1; i++) {
				page = stamper.getOverContent(i);
				rect = reader.getPageSizeWithRotation(i);
				page.beginText();
				page.setFontAndSize(bf, 11);
				page.showTextAligned(Element.ALIGN_CENTER, " - " + i + " di " + n + " -",
						rect.getRight(50), rect.getBottom(10), 0);
				page.endText();
			}
			stamper.close();
		} catch (IOException e) {
			logger.error(e.getMessage());
		} catch (DocumentException e) {
			logger.error(e.getMessage());
		}

		Iterator<String> itPercorsiPdf ;
		List<InputStream> pdfs = new ArrayList<InputStream>();
		pdfs.add(new FileInputStream(percorsoOutput + getNomeFax() + "tmp.pdf"));
		if(getAllegati()!=null && pdfUnico){
			itPercorsiPdf = getAllegati().iterator();
			while (itPercorsiPdf.hasNext()) {
				String percorsoFileAllegato="";
				try{
					percorsoFileAllegato=itPercorsiPdf.next();
					pdfs.add( new FileInputStream(percorsoFileAllegato));
				}catch(FileNotFoundException e){
					logger.warn("file " + percorsoFileAllegato +" non trovato!");
				}
			}
		}
		percorsoPdfGenerato=percorsoOutput + getNomeFax() + ".pdf";
		concatPDFs(pdfs,new FileOutputStream(percorsoPdfGenerato),false);
		for(int i = 0;i< pdfs.size();i++)
			pdfs.get(i).close();
		pdfs.clear();
		//getAllegati().clear();
		new File(percorsoOutput +  getNomeFax() + "tmp.pdf").delete();
		logger.info(getNomeFax() +":PDF GENERATO");
		if(getTipo().equals("#PDF#"))
		{
			String agenziaPraticaNome=getDestinatario();
			agenziaPraticaNome=agenziaPraticaNome.replaceAll("/", "_");
			if(Configurator.getInstance().getPropValue("pdf.save_enabled","").equals("true")){
				String dirLocale=Configurator.getInstance().getPropValue("pdf.local_path","");
				String dirArchivio=Configurator.getInstance().getPropValue("pdf.archive_path","");
				String dirLocaleFM=Configurator.getInstance().getPropValue("pdf.local_path_FM","");
				String dirArchivioFM=Configurator.getInstance().getPropValue("pdf.archive_path_FM","");
				if(dirLocale.isEmpty()!=true && dirLocaleFM.isEmpty()!= true){
					if(centroDiCosto.equals("FINEMESE")){
						new File(dirLocaleFM + "/" +  firstDir).mkdir();
						new File(dirLocaleFM + "/" + firstDir + "/" + secondDir).mkdir();
						File fileLocalCopy= new File(dirLocaleFM +  "/" + firstDir +  "/" +  secondDir + "/" + getNomeFax() + ".pdf");
						logger.info("Salvo il file in \"" + fileLocalCopy.getAbsolutePath()+ "\"");
						this.archiveUrl="/"+firstDir  + "/"+  getNomeFax() + ".pdf";
						Copier.copy(new File(percorsoPdfGenerato),fileLocalCopy);
					}else{
						//Nicolaus richesta 22.12.16
						//new File(dirLocale + "/" +  firstDir).mkdir();
						//File fileLocalCopy= new File(dirLocale +  "/" + firstDir  + "/" + getNomeFax() + ".pdf");
						//this.archiveUrl="/"+ firstDir  + "/"+  getNomeFax() + ".pdf";
						String outDir = dirLocale ;
						if(secondDir == null)
							outDir +=  "/" + firstDir;
						else
							outDir +=  "/" + firstDir + "/" + secondDir;
						new File(outDir).mkdirs();
						File fileLocalCopy= new File(outDir + "/" + getNomeFax() + ".pdf");
						logger.info("Salvo il file in \"" + fileLocalCopy.getAbsolutePath()+ "\"");
						if(secondDir != null)
							this.archiveUrl="/"+ firstDir  + "/"+  secondDir + "/" +  getNomeFax() + ".pdf";
						else
							this.archiveUrl="/"+ firstDir  + "/" +  getNomeFax() + ".pdf";
						Copier.copy(new File(percorsoPdfGenerato),fileLocalCopy);
					}
				}
				if(dirArchivio.isEmpty()!=true && dirArchivioFM.isEmpty()!=true){
					if(centroDiCosto.equals("FINEMESE")){
						new File(dirArchivioFM + "/" +  firstArchiveDir).mkdir();
						new File(dirArchivioFM + "/" + firstArchiveDir + "/" + secondArchiveDir).mkdir();
						this.archiveUrl="/"+firstArchiveDir + "/" + secondArchiveDir + "/"+  getNomeFax() + ".pdf";
						File fileArchiveCopy= new File(dirArchivioFM +"/"+ firstArchiveDir + "/" + secondArchiveDir + "/" +  getNomeFax() + ".pdf");
						logger.info("Archivio il file in \"" + fileArchiveCopy.getAbsolutePath()+ "\"");
						Copier.copy(new File(percorsoPdfGenerato),fileArchiveCopy) ;
					}else{
						new File(dirArchivio + "/" +  firstArchiveDir).mkdir();
						new File(dirArchivio + "/" + firstArchiveDir + "/" + secondArchiveDir).mkdir();
						this.archiveUrl="/"+firstArchiveDir + "/" + secondArchiveDir + "/"+  getNomeFax() + ".pdf";
						File fileArchiveCopy= new File(dirArchivio +"/"+ firstArchiveDir + "/" + secondArchiveDir + "/" +  getNomeFax() + ".pdf");
						logger.info("Archivio il file in \"" + fileArchiveCopy.getAbsolutePath()+ "\"");
						Copier.copy(new File(percorsoPdfGenerato),fileArchiveCopy) ;
					}
				}
			}
		}else if(Configurator.getInstance().getPropValue("pdf.backup", "").equals("true")) {
			Copier.copy(new File(percorsoPdfGenerato),new File(creatorBackupOutPdf.getGeneratedDirPath() + getNomeFax() + ".pdf"));
		}
		//DM Modifica richiesta da Antonello, per file inutile:
		//		if(new File(percorsoPdfGenerato).delete())
		//			logger.info("File '" + percorsoPdfGenerato + "' rimosso");
	}


	private void printLogo(Document document) throws MalformedURLException, IOException, DocumentException {
		Image jpg;
		try{
			jpg = Image.getInstance("images/"+ getLogoSRC() );
		}catch(FileNotFoundException e){
			logger.warn("Not found " + getLogoSRC() + " using default logo01.jpg");
			jpg = Image.getInstance("images/logo01.jpg");
		}
		jpg.setAlignment(Element.ALIGN_LEFT);

		if(Configurator.getInstance().getPropValue("pdf.logo_resize","true").equals("true"))
			jpg.scaleAbsolute(jpg.getWidth()/2, Integer.parseInt(Configurator.getInstance().getPropValue("pdf.logo_resize.height", "90")));

		jpg.setSpacingBefore(0);

		//LOGO ADV
		if(Configurator.getInstance().getPropValue("pdf.remote_logo","").equals("true")){
			Image logoADV = null;
			boolean isLogoExists = false;
			try{
				java.awt.Image img = getLogoADVFromUrl(nomeFax.substring(0 , nomeFax.indexOf("_")));
				if(img != null){
					logoADV = Image.getInstance(img, null);
					logoADV.setAlignment(Element.ALIGN_RIGHT);
					logoADV.scaleAbsolute(135, 90);
					logoADV.setSpacingBefore(0);
					isLogoExists= true;
				}
			}catch(Exception e){
				isLogoExists= false;
			}
			Paragraph paragraph = new Paragraph();
			paragraph.add(new Chunk(jpg,0,0,true));
			if(isLogoExists && logoADV != null)
				paragraph.add(new Chunk(logoADV, 0, 0, true));
			document.add(paragraph);
		}else{
			jpg.setAlignment(Element.ALIGN_CENTER);
			document.add(jpg);
		}
	}

	public Element stringToPhrase(String contenuto){
		contenuto = contenuto.replaceAll("�", "�");
		contenuto = contenuto.replaceAll("\n", "");

		if(contenuto.trim().equals("."))
			contenuto = contenuto.replaceAll(".", " ");

		//Per allegare TXT richiesto da EDEN
		testoDellaMail += contenuto +"\n";

		contenuto = StringUtils.rightPad(contenuto, this.maxColumns);

		Chunk ch= new Chunk(contenuto,myFont);
		Paragraph p = new Paragraph(ch);
		p.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
		p.setSpacingAfter(0);
		p.setSpacingBefore(0);
		p.setExtraParagraphSpace(0);
		p.setLeading(9.80f);

		if(this.getOrientamentoPagina().equals("V")) {

			//funzionante con FORSE 90 colonne
			if(getFontDim()==11){
				//p.setLeading(12);
			}
			//funzionante con 198 colonne
			if(getFontDim()==10){
				ch.setHorizontalScaling(0.464f);
			}
			//funzionante con 170 colonne
			if(getFontDim()==12){
				ch.setHorizontalScaling(0.45f);
			}
			//funzionante con 132 colonne
			if(getFontDim()==13){
				myFont.setSize(11);
				ch.setHorizontalScaling(0.63f);
				p.setLeading(9.855f);
			}
			//funzionante con 70 colonne
			if(getFontDim()==14){
				myFont.setSize(13.2f);
				p.setLeading(12);
			}
		}else if(this.getOrientamentoPagina().equals("O")) {

			//132 colonne
			if(getFontDim() == 10){
				myFont.setSize(13.2f);
				ch.setHorizontalScaling(0.74f);	
				p.setLeading(13.2f);
			}
			//167 colonne
			if(getFontDim() == 8){
				myFont.setSize(10.2f);
				ch.setHorizontalScaling(0.74f);		
				p.setLeading(10.2f);
			}
			//198 colonne
			if(getFontDim()==7){
				myFont.setSize(9.2f);
				ch.setHorizontalScaling(0.74f);		
				p.setLeading(9.2f);
			}
		}
		return p;
	}
	/* VECCHIA GESTIONE pre 2017 */
	/*public Phrase stringToPhrase(String contenuto){
		contenuto=contenuto.replaceAll("�", "�");
		Chunk ch= new Chunk(contenuto,myFont);
		Phrase p=new Phrase(ch);
		p.setLeading(9.855f);

		//funzionante con FORSE 90 colonne
		if(getFontDim()==11){
			//p.setLeading(12);
		}
		//funzionante con 198 colonne
		if(getFontDim()==10){
			ch.setHorizontalScaling(0.464f);
		}
		//funzionante con 170 colonne
		if(getFontDim()==12){
			ch.setHorizontalScaling(0.45f);
		}
		//funzionante con 132 colonne
		if(getFontDim()==13){
			myFont.setSize(11);
			ch= new Chunk(contenuto,myFont);
			ch.setHorizontalScaling(0.63f);
			p=new Phrase(ch);
			p.setLeading(9.855f);
		}
		if(getFontDim()==14){
			myFont.setSize(13.2f);
			p.setLeading(12);
		}
		return p;
	}*/

	public void concatPDFs(List<InputStream> streamOfPDFFiles, OutputStream outputStream, boolean paginate) {
		Document document = new Document();
		try {
			List<InputStream> pdfs = streamOfPDFFiles;
			List<PdfReader> readers = new ArrayList<PdfReader>();
			PdfCopy copy = new PdfCopy(document, outputStream);
			//			int totalPages = 0;
			Iterator<InputStream> iteratorPDFs = pdfs.iterator();

			while (iteratorPDFs.hasNext()) {
				InputStream pdf = iteratorPDFs.next();
				PdfReader pdfReader = new PdfReader(pdf);
				readers.add(pdfReader);
				//				totalPages += pdfReader.getNumberOfPages();
			}

			//			PdfWriter writer = PdfWriter.getInstance(document, outputStream);

			document.open();
			//			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			//			PdfContentByte cb = writer.getDirectContent(); // Holds the PDF
			// data

			//			PdfImportedPage page;
			//			int currentPageNumber = 0;
			int pageOfCurrentReaderPDF = 0;
			Iterator<PdfReader> iteratorPDFReader = readers.iterator();

			// Loop through the PDF files and add to the output.
			while (iteratorPDFReader.hasNext()) {
				PdfReader pdfReader = iteratorPDFReader.next();

				// Create a new page in the target for each source page.
				while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
					//					document.newPage();
					pageOfCurrentReaderPDF++;

					copy.addPage(copy.getImportedPage(pdfReader, pageOfCurrentReaderPDF));

					//					currentPageNumber++;
					//					page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
					//					cb.addTemplate(page, 0, 0);

					// Code for pagination.
					//					if (paginate) {
					//						cb.beginText();
					//						cb.setFontAndSize(bf, 9);
					//						cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "" + currentPageNumber + " of " + totalPages, 520, 5, 0);
					//						cb.endText();
					//					}
				}
				copy.freeReader(pdfReader);
				pageOfCurrentReaderPDF = 0;
			}
			outputStream.flush();
			document.close();
			outputStream.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (document.isOpen())
				document.close();
			try {
				if (outputStream != null)
					outputStream.close();
			} catch (IOException ioe) {
				logger.error(ioe.getMessage());
			}
		}
	}

	protected java.awt.Image getLogoADVFromUrl(String advCode) throws Exception{
		URL url;
		try {
			String baseUrl = Configurator.getInstance().getPropValue("pdf.remote_logo_url","");
			advCode = String.format("%1$" + 5 + "s", advCode).replace(" ", "0");
			String completeUrl = baseUrl + advCode + "/logo.jpg";
			logger.info("Getting ADV image from remote url \"" + completeUrl +"\"");
			url = new URL(completeUrl);

			URLConnection conn = url.openConnection();
			String contentType = conn.getHeaderField("Content-Type");
			logger.info("Content type is:" + contentType);


			if(!contentType.contains("application/xml")){
				//File f = new File("tmp/logo.jpg");
				java.awt.Image imgAdv = ImageIO.read(url);
				logger.info("image readed correctly");
				return imgAdv;
				//ImageIO.write(ImageIO.read(url), "JPG", f);
			}

		} catch (MalformedURLException e) {
			logger.error(e.getMessage());
			throw new MalformedURLException();
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw new IOException();
		}catch(IllegalArgumentException e){
			logger.error(e.getMessage());
			throw new IllegalArgumentException();
		}
		return null;
	}

	//Conto COLONNE
	private long getMaxColumns(){
		maxColumns = 0;
		if(getPrimaRiga() != null)
			maxColumns = getPrimaRiga().length();
		String linea = "";
		try
		{
			for(linea = getInput().readLine(); linea!=null; linea = getInput().readLine())
			{

				if(maxColumns <= linea.length())
					maxColumns = linea.length();
			}
			input.close();
			//reset buffer
			input = new BufferedReader(new FileReader(getFileSourceTXT()));
			//skip prima riga
			getInput().readLine();
			//skip testata
			getInput().readLine();
			//			getInput().readLine();

		} catch (IOException e) {
			logger.error(e.getMessage());

		}
		logger.info("Num of columns is "  + maxColumns);
		return maxColumns;
	}
	private String getHtmlTemplateString(String inputString) {	
		logger.info("Getting html template string from line " + inputString);
		String template = null;
		String numeroFile = null;
		String dataIn = null;
		String dataOut = null;

		String comodo = "";
		
		try {
			Pattern pattern = Pattern.compile(docNotesRegex);
			Matcher matcher = pattern.matcher(inputString);		
			if (matcher.matches()) {
				template = matcher.group(1);
				logger.info("found html_notes template " + template);
				numeroFile= matcher.group(2);
				logger.info("found html_notes file " + numeroFile);
				dataIn= matcher.group(3);
				logger.info("found html_notes dataIn " + dataIn);
				dataOut= matcher.group(4);
				logger.info("found html_notes dataOut " + dataOut);			
			}
			
			if(template != null) {
				Reader reader =new InputStreamReader(new FileInputStream("config/html_notes/" + template + ".html"), StandardCharsets.UTF_8);
				BufferedReader input = new BufferedReader(reader);
				for(String linea = input.readLine(); linea!=null; linea = input.readLine())
				{
					if(numeroFile != null)						
						linea = linea.replace("$$numfile$$",numeroFile);

					if(dataIn != null)
						linea = linea.replace("$$datain$$",dataIn);

					if(dataOut != null)
						linea = linea.replace("$$dataout$$",dataOut);

					comodo += linea ;
				}

				input.close();

			}else {
				logger.info("nothing found: regex not matched " + docNotesRegex);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return comodo;
	}

	public static void main(String[] args) {
		String inputStr = "&valtur;";
		//		logger.info("Getting html template string from line " + inputString);
		String template = null;
		String numeroFile = null;
		String dataIn = null;
		String dataOut = null;

		String comodo = "";
		String docNotesRegex = "&(\\w+);(\\d*);?([0-9\\-\\/]*);?([0-9\\-\\/]*);?";
		Pattern pattern = Pattern.compile(docNotesRegex);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			template = matcher.group(1);
			System.out.println("template:" + template);
			numeroFile= matcher.group(2);
			System.out.println("numeroFile:" + numeroFile );
			dataIn= matcher.group(3);
			System.out.println("dataIn:" + dataIn);
			dataOut= matcher.group(4);
			System.out.println("dataOut:" + dataOut);
		}
		if(template != null) {
			//			logger.info("found html_notes template " + template);
			if(numeroFile != null)
				//			logger.info("found html_notes file " + numeroFile);
				try {
					Reader reader =new InputStreamReader(new FileInputStream("config/html_notes/" + template + ".html"), StandardCharsets.UTF_8);
					BufferedReader input = new BufferedReader(reader);

					for(String linea = input.readLine(); linea!=null; linea = input.readLine())
					{
						if(numeroFile != null)						
							linea = linea.replace("$$numfile$$",numeroFile);

						if(dataIn != null)
							linea = linea.replace("$$datain$$",dataIn);

						if(dataOut != null)
							linea = linea.replace("$$dataout$$",dataOut);

						comodo += linea ;
					}

					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}else {
			System.out.println("nothing found: regex not matched " + docNotesRegex);
		}
		System.out.println(comodo);

		//		System.out.exit();
		//		StringBuffer newRiga = new StringBuffer();
		//		String rigaComodo = "                                               Ufficio Booking     ";
		//		String riga = "%%MOD1.PDF%%                                  NICOLAUS TOUR SRL         ";
		//		//		String riga = "%%MOD1.PDF%%                                                            ";
		//		String timeRegex = "(?<allegato>%%.+%%)";
		//		Pattern pattern = Pattern.compile(timeRegex);
		//		Matcher matcher = pattern.matcher(riga);
		//
		//		while (matcher.find()) {
		//			String allegato= matcher.group("allegato");
		//			System.out.println(allegato);
		//			String rep = StringUtils.repeat(" ",allegato.length());
		//			matcher.appendReplacement(newRiga, rep);
		//		}
		//		matcher.appendTail(newRiga);
		//		System.out.println(rigaComodo);
		//		System.out.println(newRiga.toString());		
	}

	public File getFileSourceTXT() {
		return fileSourceTXT;
	}

	public void setFileSourceTXT(File file) {
		this.fileSourceTXT = file;
	}

}
