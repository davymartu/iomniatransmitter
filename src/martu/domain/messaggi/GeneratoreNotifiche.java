/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.domain.messaggi;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import martu.utils.Configurator;
import martu.utils.Copier;
import martu.utils.CreaCartelle;

public class GeneratoreNotifiche{

	private final static Logger logger = LogManager.getLogger(GeneratoreNotifiche.class);

	private File[] f;
	private FileOutputStream fileOut;
	private PrintStream out;
	private String centroDiCosto=" ";
	private String numeroRiferimentoUno=" ";
	private String timeStampCreazione=" ";
	private static String timeStampConsegna=" ";
	private String numeroRiferimentoDue=" ";
	private String statoMessaggio=" ";
	private static final String stringaInizio="#NOT#";
	private static final String pathCopieNotifiche = "txt/Backup/Notifiche";
	private static final String pathCopieNotificheInErrore = "txt/Backup/Notifiche/error";
	private static final String pathNotificheInEntrata = "notificheIn/";
	private String durata=" ";
	private final String tentativi="000";
	public String tempFile;

	private void setStato(String string) {
		logger.info(string);
	}

	public GeneratoreNotifiche() throws IOException{
		File dir = new File(pathNotificheInEntrata);
		f = dir.listFiles();
		if(f.length==0)
			setStato("Nessuna notifica presente...");

		CreaCartelle creatorCopieNotifiche;
		CreaCartelle creatorCopieNotificheInErrore;

		creatorCopieNotifiche=new CreaCartelle(pathCopieNotifiche);
		creatorCopieNotificheInErrore=new CreaCartelle(pathCopieNotificheInErrore);

		for(int i=0;i<f.length;i++){
			if(f[i].isFile()){
				if(!isFileTest(f[i])){
					processaFile(f[i]);
				}else{
					try{
						scriviResoconto(f[i]);
					}catch(StringIndexOutOfBoundsException e){
						Copier.copy(f[i],new File(creatorCopieNotificheInErrore.getGeneratedDirPath()+ f[i].getName()));
					}
				}
				Copier.copy(f[i],new File(creatorCopieNotifiche.getGeneratedDirPath()+ f[i].getName()));
				f[i].delete();
			}
		}
	}

	private void scriviResoconto(File file) {
		tempFile=file.getName();
		try {
			BufferedReader input = new BufferedReader(new FileReader(file.getPath()));
			for(int i=1 ; i<8;i++){
				input.readLine();
			}

			String rigaJournal=input.readLine();
			if(rigaJournal.equals("UMG Journal")){
//				long dataEora = Long.parseLong(input.readLine());
				for(int i=1 ; i<8;i++){
					input.readLine();
				}

//				int numeroPratica;
//				String dept;
//				String mail;
//				String pag;
				
				String status;
				
//				String numeroKpn=tempFile;
				boolean procedi=true;
				while(procedi){
					String riga=input.readLine();

					if(riga.length()<2){
						procedi=false;
					}else{
//						numeroPratica=Integer.parseInt(riga.substring(0, 7));
//						dept=riga.substring(31,36);
//						mail=riga.substring(37,62);
//						pag=riga.substring(76,(riga.length()==79)? 79:(riga.length()==78)? 78: 77 );
						status=riga.substring(63,75);
						//System.out.println(status);
						
						if(status.matches(".*CANCELLED.*") || status.matches(".*OUTSTANDING.*")){
							try {
								System.out.println(riga);
								//								ArchivioMessaggi.insertJournal(numeroPratica, dept, mail, status, pag, numeroKpn, dataEora);
							} catch (Exception e) {
								System.err.println("errore di comunicazione con l'ODBC");
							}
						}
					}
				}

			}
			input.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean isFileTest(File file) {
		tempFile=file.getName();
		try {
			BufferedReader input = new BufferedReader(new FileReader(file.getPath()));
			for(int i=1 ; i<8;i++){
				input.readLine();
			}
			String rigaJournal=input.readLine();
			input.close();
			return (rigaJournal.equals("UMG Journal for incoming messages")||rigaJournal.equals("UMG Journal"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return false;
	}

	private void processaFile(File file) {
		try {
			tempFile=file.getName();
			BufferedReader input = new BufferedReader(new FileReader(file.getPath()));
			input.readLine();
			input.readLine();
			input.readLine();
			numeroRiferimentoUno=input.readLine();
			centroDiCosto=input.readLine();
			input.readLine();
			String timeStamp=input.readLine();
			timeStampCreazione=convertiTimeStamp(timeStamp);
			timeStamp=input.readLine();
			timeStampConsegna=convertiTimeStamp(timeStamp);
			numeroRiferimentoDue=file.getName();
			input.readLine();
			statoMessaggio=input.readLine();
			checkStatoMessaggio();
			input.readLine();
			input.readLine();
			durata=input.readLine();
			input.close();
			scriviOutNotifica();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String convertiTimeStamp(String timeStamp) {
		String mese,giorno,ora,minuti;
		mese = timeStamp.substring(2, 4);
		giorno = timeStamp.substring(4, 6);
		ora = timeStamp.substring(6, 8);
		minuti = timeStamp.substring(8, 10);
		String outString= giorno + "/" +mese + " " + ora+":" +minuti;
		return outString;
	}

	private void checkStatoMessaggio() {
		if(statoMessaggio.equals("CANCELLED ROU"))
			statoMessaggio="ROU";
		if(statoMessaggio.equals("CANCELLED NA"))
			statoMessaggio="NA";
		if(statoMessaggio.equals("CANCELLED AWC"))
			statoMessaggio="AWC";
		if(statoMessaggio.equals("CANCELLED ABS"))
			statoMessaggio="ABS";
		if(statoMessaggio.equals("CANCELLED DER"))
			statoMessaggio="DER";
		if(statoMessaggio.equals("CANCELLED E37"))
			statoMessaggio="E37";
		if(statoMessaggio.equals("CANCELLED E99"))
			statoMessaggio="E99";
		if(statoMessaggio.equals("CANCELLED INV"))
			statoMessaggio="INV";
		if(statoMessaggio.equals("CANCELLED NC"))
			statoMessaggio="NC";
		if(statoMessaggio.equals("CANCELLED NP"))
			statoMessaggio="NP";
		if(statoMessaggio.equals("CANCELLED OCC"))
			statoMessaggio="OCC";
		if(statoMessaggio.equals("CANCELLED EXP"))
			statoMessaggio="EXP";
		if(statoMessaggio.equals("CANCELLED NMX"))
			statoMessaggio="NMX";
		if(statoMessaggio.equals("CANCELLED NUS"))
			statoMessaggio="NUS";
		if(statoMessaggio.equals("CANCELLED BLA"))
			statoMessaggio="BLA";
	}

	public void scriviOutNotifica(){
		try {
			fileOut = new FileOutputStream(Configurator.getInstance().getPropValue("job.esiti.file_path", "notificheOUT/tempfile.txt"),true);
			out = new PrintStream(fileOut);

			String tempOutString=stringaInizio;
			tempOutString+=numeroRiferimentoUno;

			char[] charArray=new char[88];
			char tempOutArray[]=tempOutString.toCharArray();
			for(int i=0;i<charArray.length;i++){
				charArray[i]=' ';
			}
			try{
				for(int i=0;i<tempOutArray.length;i++){
					charArray[i]= tempOutArray[i];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				tempOutArray=numeroRiferimentoDue.toCharArray();
				for(int i=15;i<tempOutArray.length+15;i++){
					charArray[i]= tempOutArray[i-15];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				tempOutArray=timeStampConsegna.toCharArray();
				for(int i=35;i<tempOutArray.length+35;i++){
					charArray[i]= tempOutArray[i-35];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				tempOutArray=timeStampCreazione.toCharArray();
				for(int i=50;i<tempOutArray.length+50;i++){
					charArray[i]= tempOutArray[i-50];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				tempOutArray=statoMessaggio.toCharArray();
				for(int i=65;i<tempOutArray.length+65;i++){
					charArray[i]= tempOutArray[i-65];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				tempOutArray=durata.toCharArray();
				for(int i=68;i<tempOutArray.length+68;i++){
					charArray[i]= tempOutArray[i-68];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				tempOutArray=tentativi.toCharArray();
				for(int i=75;i<tempOutArray.length+75;i++){
					charArray[i]= tempOutArray[i-75];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}

			tempOutArray=centroDiCosto.toCharArray();
			try{
				for(int i=78;i<=charArray.length;i++){
					charArray[i]= tempOutArray[i-78];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			tempOutString="";
			for(int i=0;i<charArray.length;i++){
				tempOutString+=charArray[i];
			}
			out.println(tempOutString);
			setStato(tempOutString);
			out.close();
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	public static void scriviNotificaLocale(String data,String ora,String protocollo,String tipo, String errore,String epdf,String email,String centroDiCosto, String dataOrigine){
		try {
			FileOutputStream fileOut = new FileOutputStream(Configurator.getInstance().getPropValue("job.esiti.file_path", "notificheOUT/tempfile.txt"),true);
			PrintStream out = new PrintStream(fileOut,true);

			String tempOutString=stringaInizio;
			tempOutString+=protocollo;

			char[] charArray=new char[88];
			char tempOutArray[]=tempOutString.toCharArray();
			for(int i=0;i<charArray.length;i++){
				charArray[i]=' ';
			}
			try{
				for(int i=0;i<tempOutArray.length;i++){
					charArray[i]= tempOutArray[i];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				tempOutArray=protocollo.toCharArray();
				for(int i=15;i<tempOutArray.length+15;i++){
					charArray[i]= tempOutArray[i-15];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				String dataConsegna=data + " " + ora;
				tempOutArray=dataConsegna.toCharArray();
				for(int i=35;i<tempOutArray.length+35;i++){
					charArray[i]= tempOutArray[i-35];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				tempOutArray=dataOrigine.toCharArray();
				for(int i=50;i<tempOutArray.length+50;i++){
					charArray[i]= tempOutArray[i-50];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				tempOutArray=errore.toCharArray();
				for(int i=65;i<tempOutArray.length+65;i++){
					charArray[i]= tempOutArray[i-65];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				String durata="0000000";
				tempOutArray=durata.toCharArray();
				for(int i=68;i<tempOutArray.length+68;i++){
					charArray[i]= tempOutArray[i-68];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			try{
				String tentativi="001";
				tempOutArray=tentativi.toCharArray();
				for(int i=75;i<tempOutArray.length+75;i++){
					charArray[i]= tempOutArray[i-75];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}

			tempOutArray=centroDiCosto.toCharArray();
			try{
				for(int i=78;i<=charArray.length;i++){
					charArray[i]= tempOutArray[i-78];
				}
			}catch(ArrayIndexOutOfBoundsException e){

			}
			tempOutString="";
			for(int i=0;i<charArray.length;i++){
				tempOutString+=charArray[i];
			}
			out.println(tempOutString);		
			if(out.checkError()) {
				logger.warn("Something was wrong with notifiche PrintStream");
			}
			out.close();
			fileOut.close();
			logger.info(tempOutString);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}catch (NullPointerException e) {
			logger.error("Errore formato file notifica");

		}
	}
	
	public static void main(String args[]) {
		SimpleDateFormat data_format=new SimpleDateFormat("dd/MM");
		String dataDB=data_format.format(new GregorianCalendar().getTime());
		SimpleDateFormat ora_format =new SimpleDateFormat("HH:mm:ss");
		String oraDB=ora_format.format(new GregorianCalendar().getTime());
		String data ="19-06-19 16:30";
		String dataOrigine=data.substring(0,2)+"/"+data.substring(3,5)+" " +data.substring(9,14);
		GeneratoreNotifiche.scriviNotificaLocale(dataDB, oraDB, "I5000003",  "#INT#", "ERR","1","0","MTS",dataOrigine);
	}

}
