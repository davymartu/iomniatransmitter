/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.timer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TimerSlotCounter extends java.util.Observable implements ActionListener{
	int mm=0;
	int ss=0;
	int timeout;


	/**
	 * @return the mm
	 */
	String getMm() {
		if(timeout>=0){			
			mm=timeout/60;
			if(mm<10)
				return "0" + mm;
			return "" + mm;
		}
		return "00";
	}	

	/**
	 * @return the ss
	 */
	String getSs() {
		if(timeout>=0){			
			ss=timeout%60;
			if(ss<10)
				return "0" + ss;
			return "" + ss;

		}
		return "00";
	}	

	public void setTimeout(int ss){
		this.timeout=ss;
		this.setChanged();
		this.notifyObservers();
	}

	@Override
	public void actionPerformed(ActionEvent e) {	
		if(timeout > 0){
			timeout--;
			this.setChanged();
			this.notifyObservers();
		}

	}
}
