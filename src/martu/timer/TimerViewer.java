/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.timer;

import java.util.Observable;

import javax.swing.JLabel;

public class TimerViewer extends JLabel implements java.util.Observer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3537493205466683952L;
	public TimerViewer(){
		setText("0 giorni 00:00:00");
	}
	@Override
	public void update(Observable o, Object arg) {
		setText(((TimerCounter)o).getGg() + " giorni "+
				((TimerCounter)o).getHh() + ":" +
				((TimerCounter)o).getMm() + ":" +
				((TimerCounter)o).getSs());
	}
}
