/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.timer;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;

public class TimerSlotViewer extends JLabel implements Observer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7333355036296345021L;
	
	public TimerSlotViewer(){
		setText("00:00");
	}		

	@Override
	public void update(Observable o, Object arg1) {
		setText(((TimerSlotCounter)o).getMm() + ":" +
				((TimerSlotCounter)o).getSs());
		
	}
}
