/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.timer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TimerCounter extends java.util.Observable implements ActionListener{
	int mm=0;
	int ss=0;
	long hh=0;
	long gg=0;
	/**
	 * @return the mm
	 */
	String getMm() {
		if(mm<10)
			return "0" + mm;
		return "" + mm;
	}
	
	/**
	 * @return the ss
	 */
	String getSs() {
		if(ss<10)
			return "0" + ss;
		return "" + ss;
	}
	
	/**
	 * @return the hh
	 */
	String getHh() {
		if(hh<10)
			return "0" + hh;
		return "" + hh;
	}
	
	/**
	 * @return the gg
	 */
	String getGg() {
		return "" + gg;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ss++;
		if(ss==60){
			mm++;
			ss=0;
		}
		if(mm==60){
			hh++;
			mm=0;
		}
		if(hh==24){
			gg++;
			hh=0;
		}
		this.setChanged();
		this.notifyObservers();
	}
	
	
}
