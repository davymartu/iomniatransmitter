/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.ui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import martu.domain.controller.Controller;

public class MyShutdownHook extends Thread {
	private static final Logger logger = LogManager.getLogger(MyShutdownHook.class);
	@Override
	public void run() {
		logger.info("shutdown hook activated");
		Controller.getInstance().stop();
	}
}
