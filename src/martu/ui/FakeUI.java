/*
 * iOmniaTransmitter - Communication Engine 
 * Copyright (C) 2019 Davide Martusciello
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *   
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package martu.ui;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import martu.domain.controller.Controller;
import martu.domain.controller.FaxComposerDigitalDoxStrategy;
import martu.domain.controller.GenericDocumentStrategy;
import martu.domain.controller.SmtpModeStrategy;
import martu.utils.Configurator;

public class FakeUI {

	private static final Logger logger = LogManager.getLogger(FakeUI.class);

	public static void main(String[] args) {
		LoggerContext context = (LoggerContext) LogManager.getContext(false);
		File file = new File("config/log4j2.properties");
		context.setConfigLocation(file.toURI());		

		try{
			@SuppressWarnings("resource")
			RandomAccessFile randomFile = new RandomAccessFile(".lock","rw");

			FileChannel channel = randomFile.getChannel();

			if(channel.tryLock() == null) 
				logger.error("Another instance already Running...exit!");
			else {
				logger.info("Avvio in corso...");

				////TEST ROLLOVER
//				org.apache.logging.log4j.core.Logger loggerImpl = (org.apache.logging.log4j.core.Logger) logger;
//				RollingRandomAccessFileAppender appender = (RollingRandomAccessFileAppender) loggerImpl.getAppenders().get("rollingrandomaccessfile");
//				appender.getManager().rollover();
			
				if(Configurator.getInstance().getPropValue("job.smtp","false").equals("true"))
					Controller.getInstance().avvia(new SmtpModeStrategy());

				if(Configurator.getInstance().getPropValue("job.documents","false").equals("true"))
					Controller.getInstance().avvia(new GenericDocumentStrategy());
				
				if(Configurator.getInstance().getPropValue("job.fax","false").equals("true"))
					Controller.getInstance().avvia(new FaxComposerDigitalDoxStrategy());

				//				Runtime.getRuntime().addShutdownHook(new MyShutdownHook());
			}
		}catch( Exception e ) { 
			logger.error(e.getMessage(),e);
		}
	}
}
