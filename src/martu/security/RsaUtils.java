package martu.security;

import javax.crypto.Cipher;

import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.bouncycastle.util.io.pem.PemWriter;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;


import static java.nio.charset.StandardCharsets.UTF_8;

public class RsaUtils {
	final static String FOLDER = "config/security/";
	final static String ALGORITHM = "RSA";
	
	public static KeyPair generateKeyPair() throws Exception {
		KeyPairGenerator generator = KeyPairGenerator.getInstance(ALGORITHM);
		generator.initialize(2048, new SecureRandom());
		KeyPair pair = generator.generateKeyPair();

		return pair;
	}

	public static String encrypt(String plainText, PublicKey publicKey) throws Exception {
		Cipher encryptCipher = Cipher.getInstance("RSA");
		encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

		byte[] cipherText = encryptCipher.doFinal(plainText.getBytes(UTF_8));

		return Base64.getEncoder().encodeToString(cipherText);
	}

	public static String decrypt(String cipherText, PrivateKey privateKey) throws Exception {
		byte[] bytes = Base64.getDecoder().decode(cipherText);

		Cipher decriptCipher = Cipher.getInstance("RSA");
		decriptCipher.init(Cipher.DECRYPT_MODE, privateKey);

		return new String(decriptCipher.doFinal(bytes), UTF_8);
	}

	public static String sign(String plainText, PrivateKey privateKey) throws Exception {
		Signature privateSignature = Signature.getInstance("SHA256withRSA");
		privateSignature.initSign(privateKey);
		privateSignature.update(plainText.getBytes(UTF_8));

		byte[] signature = privateSignature.sign();

		return Base64.getEncoder().encodeToString(signature);
	}

	public static boolean verify(String plainText, String signature, PublicKey publicKey) throws Exception {
		Signature publicSignature = Signature.getInstance("SHA256withRSA");
		publicSignature.initVerify(publicKey);
		publicSignature.update(plainText.getBytes(UTF_8));

		byte[] signatureBytes = Base64.getDecoder().decode(signature);

		return publicSignature.verify(signatureBytes);
	}

	public static void writePemFile(Key key, String description, String fileName) throws IOException {    	
		PemObject pemKey = new PemObject(description, key.getEncoded());
		PemWriter pemWriter = new PemWriter(new OutputStreamWriter(new FileOutputStream(FOLDER + fileName)));
		try {
			pemWriter.writeObject(pemKey);
		} finally {
			pemWriter.close();
		}		
	}    

	public static void SaveKeyPair(String certName, KeyPair keyPair) throws IOException {

		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();

		writePemFile(privateKey, "RSA PRIVATE KEY", certName);
		writePemFile(publicKey, "RSA PUBLIC KEY", certName + ".pub");

	}

	public static KeyPair LoadKeyPair(String keyFileName)
			throws IOException, NoSuchAlgorithmException,
			InvalidKeySpecException {
		KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
		PemReader pemReader = new PemReader(new FileReader(FOLDER + keyFileName));
		PemObject pemObject = pemReader.readPemObject();
		byte[] pemContent = pemObject.getContent();
		pemReader.close();
		
		PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(pemContent);
		PrivateKey privateKey = keyFactory.generatePrivate(encodedKeySpec);
		
		pemReader = new PemReader(new FileReader(FOLDER + keyFileName + ".pub"));
		pemObject = pemReader.readPemObject();
		pemContent = pemObject.getContent();
		pemReader.close();
		
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(pemContent);
		PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);		

		return new KeyPair(publicKey, privateKey);
	}

	public static void main(String... argv) throws Exception {
		String keyFileName= "iom_cert";
		//First generate a public/private key pair
		//KeyPair pair = generateKeyPair();
		//System.out.println("Generated Key Pair");

		//SaveKeyPair(keyFileName,pair);
		System.out.println("L1");
		KeyPair pair = LoadKeyPair(keyFileName);
		System.out.println("L2");
		//Our secret message
		String message = "the answer to life the universe and everything";
		
		//Encrypt the message
		System.out.println("E1");
		String cipherText = encrypt(message, pair.getPublic());
		System.out.println(cipherText);
		System.out.println("E2");
		
		//Let's sign our message
		System.out.println("S1");
		String signature = sign("foobar", pair.getPrivate());
		System.out.println("S1");
		
		//Load
		//pair = LoadKeyPair(keyFileName);
		
		//Now decrypt it
		System.out.println("D1");
		String decipheredMessage = decrypt(cipherText, pair.getPrivate());
		System.out.println("D2");
		System.out.println(decipheredMessage);
		
		//Let's check the signature
		boolean isCorrect = verify("foobar", signature, pair.getPublic());
		System.out.println("Signature correct: " + isCorrect);
	}
}